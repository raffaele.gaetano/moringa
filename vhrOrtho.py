import otbApplication as otb
import gdal
import glob
import sys
import os

def tileFusion(fld,fld_out):
    lst = sorted(glob.glob(fld + os.sep + '*_R*C*.TIF'))
    R, C = int(lst[-1][-7]), int(lst[-1][-5])

    basename = fld_out + os.sep + os.path.basename(lst[0])[:-9]

    tf = otb.Registry.CreateApplication('TileFusion')
    tf.SetParameterInt('rows', R)
    tf.SetParameterInt('cols', C)
    tf.SetParameterStringList('il', lst)
    tf.SetParameterString('out', basename + '_FULL.TIF?&gdal:co:compress=deflate&gdal:co:bigtiff=yes')
    tf.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint16)
    tf.ExecuteAndWriteOutput()

def rasterSensorToTOA(fld):
    toa = otb.Registry.CreateApplication('OpticalCalibration')
    mil = otb.Registry.CreateApplication('BandMathX')

    fns = glob.glob(fld + os.sep + '*_FULL.TIF')
    if len(fns) == 1:
        fn = fns[0]
        toa.SetParameterString('in', fn)
        toa.SetParameterInt('clamp', 0)
        toa.SetParameterInt('milli', 0)
        #toa.SetParameterString('out', basename + 'FULL_TOA.TIF')
        toa.Execute()
        mil.AddImageToParameterInputImageList('il',toa.GetParameterOutputImage('out'))
        mil.SetParameterString('exp','im1*10000')
        mil.SetParameterString('out', fn.replace('.TIF','_TOA.TIF?&gdal:co:compress=deflate&gdal:co:bigtiff=yes'))
        mil.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint16)
        mil.ExecuteAndWriteOutput()
    else:
        sys.exit('Perform tile fusion first.')

    return fn.replace('.TIF', '_TOA.TIF')

def tileFusionTOA(fld, fld_out):
    lst = sorted(glob.glob(fld + os.sep + '*_R*C*.TIF'))
    if len(lst) == 0:
        lst = sorted(glob.glob(fld + os.sep + '*_R*C*.JP2'))
    R, C = int(lst[-1][-7]), int(lst[-1][-5])

    basename = fld_out + os.sep + os.path.basename(lst[0])[:-9]
    finalname = basename + '_FULL_TOA.TIF'

    if os.path.exists(finalname):
        return finalname
    else:
        tf = otb.Registry.CreateApplication('TileFusion')
        tf.SetParameterInt('rows', R)
        tf.SetParameterInt('cols', C)
        tf.SetParameterStringList('il', lst)
        tf.Execute()

        toa = otb.Registry.CreateApplication('OpticalCalibration')
        toa.SetParameterInputImage('in', tf.GetParameterOutputImage('out'))
        toa.SetParameterInt('clamp', 0)

        toa.SetParameterInt('milli', 1)
        toa.SetParameterString('out', basename + '_FULL_TOA.TIF?&gdal:co:compress=deflate&gdal:co:bigtiff=yes')
        toa.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint16)
        toa.ExecuteAndWriteOutput()
        '''
        toa.SetParameterInt('milli', 0)
        toa.Execute()

        mil = otb.Registry.CreateApplication('BandMathX')
        mil.AddImageToParameterInputImageList('il', toa.GetParameterOutputImage('out'))
        mil.SetParameterString('exp', 'im1*10000')
        mil.SetParameterString('out', basename + '_FULL_TOA.TIF?&gdal:co:compress=deflate&gdal:co:bigtiff=yes')
        mil.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint16)
        mil.ExecuteAndWriteOutput()
        '''

        return finalname


def makeOrtho(fld,epsg_code,dem_fld,grid_spacing,skip_carto=False,geoid_fn=None):
    ort = otb.Registry.CreateApplication('OrthoRectification')
    fn = glob.glob(fld + os.sep + '*_FULL_TOA.TIF')
    extfn = ''
    if skip_carto:
        extfn = '?&skipcarto=true'
    if len(fn) == 1:
        fn = fn[0]
        ort.SetParameterString('io.in', fn + extfn)
        ort.SetParameterString('map', 'epsg')
        ort.SetParameterString('map.epsg.code', str(epsg_code))
        ort.SetParameterString('elev.dem', dem_fld)
        if geoid_fn is not None:
            ort.SetParameterString('elev.geoid', geoid_fn)
        ort.SetParameterString('opt.gridspacing', str(grid_spacing))
        ort.SetParameterString('io.out', fn.replace('.TIF','_ORTHO.TIF?&gdal:co:compress=deflate&gdal:co:bigtiff=yes'))
        ort.SetParameterOutputImagePixelType('io.out', otb.ImagePixelType_uint16)
        ort.ExecuteAndWriteOutput()

def makeOrthoFit(fld,fit_img,dem_fld,grid_spacing,skip_carto=False,geoid_fn=None):
    ort = otb.Registry.CreateApplication('OrthoRectification')
    fn = glob.glob(fld + os.sep + '*_FULL_TOA.TIF')
    extfn = '?geom=' + os.path.join(fld,'refined.geom')
    if skip_carto:
        extfn += '&skipcarto=true'
    if len(fn) == 1:
        fn = fn[0]
        ort.SetParameterString('io.in', fn + extfn)
        ort.SetParameterString('outputs.mode', 'orthofit')
        ort.SetParameterString('outputs.ortho', fit_img)
        ort.SetParameterString('elev.dem', dem_fld)
        if geoid_fn is not None:
            ort.SetParameterString('elev.geoid', geoid_fn)
        ort.SetParameterString('opt.gridspacing', str(grid_spacing))
        ort.SetParameterString('io.out', fn.replace('.TIF','_ORTHO.TIF?&gdal:co:compress=deflate&gdal:co:bigtiff=yes'))
        ort.SetParameterOutputImagePixelType('io.out', otb.ImagePixelType_uint16)
        ort.ExecuteAndWriteOutput()

def pansharp(fld_pan, fld_ms, fld_out, phr=False):
    psh = otb.Registry.CreateApplication('BundleToPerfectSensor')
    pan = glob.glob(fld_pan + os.sep + '*_P_*_FULL_TOA_ORTHO.TIF')
    ms = glob.glob(fld_ms + os.sep + '*_MS_*_FULL_TOA_ORTHO.TIF')
    if len(pan) == 1 and len(ms) == 1:
        pan = pan[0]
        ms = ms[0]
        psh.SetParameterString('inp',pan)
        psh.SetParameterString('inxs',ms)
        if phr:
            psh.SetParameterString('mode','phr')
        psh.SetParameterString('out',fld_out + os.sep + os.path.basename(ms).replace('_MS_','_PS_') + '?&gdal:co:compress=deflate&gdal:co:bigtiff=yes')
        psh.SetParameterString('method','bayes')
        psh.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint16)
        psh.ExecuteAndWriteOutput()
    else:
        sys.exit('Found multiple PAN and/or MS. Check.')

def refineGeometry(img, ref, dem_fld, geoid, epsg, out_geom, skc=False, prec=3):
    if os.path.exists(out_geom):
        return
    else:
        hpe = otb.Registry.CreateApplication('HomologousPointsExtraction')
        if skc:
            hpe.SetParameterString('in1', img + '?skipcarto=true')
        else:
            hpe.SetParameterString('in1', img)
        hpe.SetParameterString('in2', ref)
        hpe.SetParameterString('out', os.path.join(os.path.dirname(img), 'points.txt'))
        hpe.SetParameterString('mode', 'geobins')
        hpe.SetParameterString('algorithm', 'sift')
        hpe.SetParameterInt('backmatching', 1)
        hpe.SetParameterInt('precision', prec)
        hpe.SetParameterInt('mfilter', 1)
        hpe.SetParameterInt('2wgs84', 1)
        hpe.SetParameterString('elev.dem', dem_fld)
        hpe.SetParameterString('elev.geoid', geoid)
        hpe.ExecuteAndWriteOutput()

        rsm = otb.Registry.CreateApplication('RefineSensorModel')
        rsm.SetParameterString('ingeom', os.path.splitext(img)[0]+'.geom')
        rsm.SetParameterString('inpoints', os.path.join(os.path.dirname(img), 'points.txt'))
        rsm.SetParameterString('outgeom', out_geom)
        rsm.SetParameterString('map', 'epsg')
        rsm.SetParameterString('map.epsg.code', str(epsg))
        rsm.SetParameterString('elev.dem', dem_fld)
        rsm.SetParameterString('elev.geoid', geoid)
        rsm.ExecuteAndWriteOutput()

        return

def parse_command(argv=sys.argv[1:]):
    import argparse
    parser = argparse.ArgumentParser(prog='vhrOrtho', description='Performs preprocessing steps on SPOT6/7 and Pleiades imagery in raster sensor geometry.')
    parser.add_argument('pan_folder', help='Folder containing the Panchromatic image.')
    parser.add_argument('ms_folder', help='Folder containing the Multispectral image.')
    parser.add_argument('dem_folder', help='Folder containing DEM tiles in WGS84 coordinate system.')
    parser.add_argument('geoid_file', help='Path of the geoid file (ex. egm96.grd).')
    parser.add_argument('output_folder', help='Folder where output images will be stored')
    parser.add_argument('--pleiades', action='store_true', help='Set this option if Pleiades imagery is processed. Default is SPOT6/7')
    parser.add_argument('--pansharp', action='store_true', help='Set this option if final pansharpening has to be performed.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--orthoref', help='Folder containing PAN/MS ortho reference images for precise co-registration (geometry will be inherited).')
    group.add_argument('--epsg', type=int, help='EPGS code for output image. Ignored if orthoref is specified.')
    args = parser.parse_args(argv)
    return args


if __name__ == '__main__':

    args = parse_command()
    # Syntax: vhrOrtho.py <folder_PAN> <folder_MS> <folder_DEM> <folder_out> <epsg_out> <geoid_fn>
    # Configured for SPOT6/7
    if args.pleiades:
        prec = [36, 9]
        gsp = [2, 8]
        skc = False
    else:
        prec = [12, 3]
        gsp = [6, 24]
        skc = True

    of = args.output_folder
    opf = os.path.join(of, 'PAN')
    omsf = os.path.join(of,'MS')
    opsf = os.path.join(of, 'PS')
    for f in [of,omsf,opf,opsf]:
        if not os.path.exists(f):
            os.mkdir(f)

    #tileFusion(args.pan_folder, opf)
    #tileFusion(args.ms_folder, omsf)

    #pfn = rasterSensorToTOA(opf)
    #msfn = rasterSensorToTOA(omsf)

    pfn = tileFusionTOA(args.pan_folder,opf)
    msfn = tileFusionTOA(args.ms_folder,omsf)

    if args.orthoref is not None:
        pref = glob.glob(os.path.join(args.orthoref,'PAN','*_TOA_ORTHO.TIF'))[0]
        msref = glob.glob(os.path.join(args.orthoref, 'MS', '*_TOA_ORTHO.TIF'))[0]
        epsg = gdal.Open(msref).GetProjection().split('"')[-2]
        refineGeometry(pfn, pref, args.dem_folder, args.geoid_file, epsg, os.path.join(opf, 'refined.geom'), skc, prec[0])
        refineGeometry(msfn, msref, args.dem_folder, args.geoid_file, epsg, os.path.join(omsf, 'refined.geom'), skc, prec[1])
        makeOrthoFit(opf, pref, args.dem_folder, gsp[0], skc, args.geoid_file)
        makeOrthoFit(omsf, msref, args.dem_folder, gsp[1], skc, args.geoid_file)
    else:
        makeOrtho(opf, args.epsg, args.dem_folder, gsp[0], skc, args.geoid_file)
        makeOrtho(omsf, args.epsg, args.dem_folder, gsp[1], skc, args.geoid_file)

    if args.pansharp:
        pansharp(opf, omsf, opsf, phr=args.pleiades)
    else:
        os.rmdir(opsf)
