import otbApplication as otb

def scl2mask(scl_file):

    #expr1 = 'im1b1 == 8 || im1b1 == 9 || im1b1 == 10'
    expr1 = 'im1b1 == 8 || im1b1 == 9'
    #expr2 = '1 - ((im1b1 == 3 || im1b1 == 8 || im1b1 == 9 || im1b1 == 10 || im1b1 == 2 || im1b1 == 7) * im2b1)'
    expr2 = '1 - ((im1b1 == 3 || im1b1 == 8 || im1b1 == 9 || im1b1 == 2 || im1b1 == 7) * im2b1)'

    clr = otb.Registry.CreateApplication('ClassificationMapRegularization')
    clr.SetParameterString('io.in', scl_file)
    clr.SetParameterInt('ip.radius', 5)
    clr.Execute()

    bm1 = otb.Registry.CreateApplication('BandMath')
    bm1.AddImageToParameterInputImageList('il', clr.GetParameterOutputImage('io.out'))
    #bm1.SetParameterStringList('il', [scl_file])
    bm1.SetParameterString('exp', expr1)
    bm1.Execute()

    """
    ero = otb.Registry.CreateApplication('BinaryMorphologicalOperation')
    ero.SetParameterInputImage('in', bm1.GetParameterOutputImage('out'))
    ero.SetParameterString('structype', 'ball')
    ero.SetParameterInt('xradius', 2)
    ero.SetParameterInt('yradius', 2)
    ero.SetParameterString('filter', 'erode')
    ero.Execute()
    """

    dil = otb.Registry.CreateApplication('BinaryMorphologicalOperation')
    dil.SetParameterInputImage('in', bm1.GetParameterOutputImage('out'))
    dil.SetParameterString('structype', 'ball')
    dil.SetParameterInt('xradius', 100)
    dil.SetParameterInt('yradius', 100)
    dil.SetParameterString('filter', 'dilate')
    dil.Execute()

    bm2 = otb.Registry.CreateApplication('BandMath')
    #bm2.SetParameterStringList('il', [scl_file])
    bm2.AddImageToParameterInputImageList('il', clr.GetParameterOutputImage('io.out'))
    bm2.AddImageToParameterInputImageList('il', dil.GetParameterOutputImage('out'))
    bm2.SetParameterString('exp', expr2)
    bm2.Execute()


    fdl = otb.Registry.CreateApplication('BinaryMorphologicalOperation')
    fdl.SetParameterInputImage('in', bm2.GetParameterOutputImage('out'))
    fdl.SetParameterString('structype', 'ball')
    fdl.SetParameterInt('xradius', 5)
    fdl.SetParameterInt('yradius', 5)
    fdl.SetParameterString('filter', 'erode')
    fdl.SetParameterString('out', scl_file.replace('_SCL_', '_TMPMASK_'))
    fdl.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint8)
    fdl.ExecuteAndWriteOutput()

    """
    opn = otb.Registry.CreateApplication('BinaryMorphologicalOperation')
    opn.SetParameterInputImage('in', bm2.GetParameterOutputImage('out'))
    opn.SetParameterString('structype', 'ball')
    opn.SetParameterInt('xradius', 5)
    opn.SetParameterInt('yradius', 5)
    opn.SetParameterString('filter', 'opening')
    opn.SetParameterString('out', scl_file.replace('_SCL_', '_GAPMASK_'))
    opn.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint8)
    opn.ExecuteAndWriteOutput()
    """

    return