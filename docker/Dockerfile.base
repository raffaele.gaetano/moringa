ARG BASE_IMG=ubuntu:20.04
# Base image can be either focal (20.04) or bionic (18.04) and descendants
FROM $BASE_IMG
LABEL description="A base image with OTB and remote modules for the MORINGA processing chain, CIRAD/UMR TETIS"

WORKDIR /tmp

# Controls USE_SYSTEM_* variables
ARG USE_SYSTEM_DEPS=false
# DEPS_INSTALL_PREFIX=/usr is required when USE_SYSTEM_DEPS=true
ARG DEPS_INSTALL_PREFIX=/opt/otb

# Install build dependencies
RUN apt-get update \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get install -y --no-install-recommends \
        bzip2 \
        cmake \
        curl \
        file \
        g++ \
        gcc \
        git \
        make \
        nano \
        patch \
        pkg-config \
        python3-dev \
        python3-numpy \
        python3-pip \
        python3-setuptools \
        python3-sklearn \
        python3-wheel \
        swig \
        unzip \
        vim \
        wget \
        zip \
    && if $USE_SYSTEM_DEPS; then \
        apt-get install -y --no-install-recommends \
            bison \
            gdal-bin \
            python3-gdal \
            libboost-date-time-dev \
            libboost-filesystem-dev \
            libboost-graph-dev \
            libboost-program-options-dev \
            libboost-system-dev \
            libboost-thread-dev \
            libcurl4-gnutls-dev \
            libexpat1-dev \
            libfftw3-dev \
            libgdal-dev \
            libgeotiff-dev \
            libgsl-dev \
            libinsighttoolkit4-dev \
            libkml-dev \
            libmuparser-dev \
            libmuparserx-dev \
            libopencv-core-dev \
            libopencv-ml-dev \
            libopenthreads-dev \
            libossim-dev \
            libpng-dev \
            libsvm-dev \
            libtinyxml-dev \
            zlib1g-dev; fi \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# OTB branch to clone
ARG OTB_BRANCH=release-7.2
# Set USE_SYSTEM_* flags
COPY docker/build-flags-otb.txt .
RUN if ! $USE_SYSTEM_DEPS; then \
        sed -i -r "s/-DUSE_SYSTEM_([A-Z0-9]*)=ON/-DUSE_SYSTEM_\1=OFF/" build-flags-otb.txt ; fi

# SuperBuild OTB
RUN mkdir /tmp/SuperBuild-archives /tmp/build /opt/otb \
    && git clone --single-branch -b $OTB_BRANCH "https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb.git" \
    && if ! $USE_SYSTEM_DEPS; then \
        cd SuperBuild-archives \
        && OTB_VERSION=$(head -n1 /tmp/otb/RELEASE_NOTES.txt | sed -r 's/OTB-v ([0-9].[0-9]).[0-9] -.*/\1/') \
        && curl -s "https://www.orfeo-toolbox.org/packages/archives/OTB/SuperBuild-archives-$OTB_VERSION.tar.bz2" \
            | tar -xjv --exclude 'q*' --exclude '*gl*'; fi \
    && cd /tmp/build \
    && OTB_CMAKE_FLAGS=$(cat "../build-flags-otb.txt") \
    && cmake /tmp/otb/SuperBuild \
        $OTB_CMAKE_FLAGS \
        -DCMAKE_BUILD_TYPE="Release" \
        -DDOWNLOAD_LOCATION=/tmp/SuperBuild-archives \
        -DCMAKE_INSTALL_PREFIX=/opt/otb \
    && make OTB_DEPENDS -j12 \
    && cd /tmp/otb/Modules/Remote \
    && rm -f otbGRM.remote.cmake \
    && git clone "https://gitlab.irstea.fr/remi.cresson/GRM.git" \
    && git clone "https://gitlab.irstea.fr/remi.cresson/LSGRM.git" \
    && git clone "https://gitlab.irstea.fr/raffaele.gaetano/otbVectorClassification.git" \
    && git clone "https://gitlab.irstea.fr/raffaele.gaetano/otbSelectiveHaralickTextures.git" \
    && cd /tmp/build/OTB/build \
    && cmake /tmp/otb \
        -DModule_otbGRM=ON \
        -DModule_LSGRM=ON \
        -DModule_OTBAppSelectiveHaralickTextures=ON \
        -DModule_OTBAppVectorFeaturesStatistics=ON \
        -DModule_OTBTemporalGapFilling=ON \
        -DModule_S1TilingSupportApplications=ON \
        -DOTB_WRAP_PYTHON=ON \
        -DPYTHON_EXECUTABLE=/usr/bin/python3 \
        -DCMAKE_INSTALL_PREFIX=/opt/otb \
    && cd /tmp/build \
    && make -j8 \
    && rm -rf /tmp/* /root/.cache /opt/otb/bin/otbgui_*

# Persistent environment variables
ENV PATH="/opt/otb/bin:$PATH"
ENV PYTHONPATH="/opt/otb/lib/python3/dist-packages:/opt/otb/lib/otb/python:$PYTHONPATH"
ENV LD_LIBRARY_PATH="/opt/otb/lib:$LD_LIBRARY_PATH"
ENV OTB_APPLICATION_PATH="/opt/otb/lib/otb/applications"
ENV PROJ_LIB="$DEPS_INSTALL_PREFIX/share/proj"
ENV GDAL_DATA="$DEPS_INSTALL_PREFIX/share/gdal"
