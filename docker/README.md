# Docker overview
## Installation

*Windows 10*  
```
Install Docker Desktop then use cmd.exe to execute docker pull and docker create.
```

*Debian and Ubuntu*  
See full documentation here : https://docs.docker.com/engine/install/ubuntu/
```bash
# Uninstall old versions (an old docker version is available via apt default repositories)
sudo apt-get remove docker docker-engine docker.io containerd runc

# System dependencies
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# Get Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Add Docker apt repository
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install latest Docker Community Edition
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# Allow user to run docker commands, see https://docs.docker.com/engine/install/linux-postinstall/
# Only if you want to avoid using sudo for each docker command ; docker group grants privileges equivalent to the root user.
sudo groupadd docker
sudo usermod -aG docker $USER

# Test docker run
docker run hello-world

# Useful commands
docker info         # System info
docker images       # List local images
docker container ls # List containers
docker ps           # Show running containers

# Control state with systemd
#sudo systemctl {status,enable,disable,start,stop} docker
```

## Usage
### Pull, create, exec...
N.B. : in previous docker images, default user was **moringa** and every python files were located in `/home/moringa`  
In newer images (tag develop, now based on ubuntu:20.04), default user is **ubuntu** and you'll find evey python files in `/home/ubuntu/moringa`

```bash
# Pull image (default tag is "latest")
docker pull gitlab-registry.irstea.fr/raffaele.gaetano/moringa

# Simple command in a one-shot container (anonymous container will not persist)
docker run gitlab-registry.irstea.fr/raffaele.gaetano/moringa otbcli_BandMathX

# Persistent (named) container with volume, here with home dir, but it can be any directory
# Beware of ownership issues, see the last section of this doc
docker create --interactive --tty --volume /home/$USER:/home/ubuntu/data \
    --name moringa gitlab-registry.irstea.fr/raffaele.gaetano/moringa /bin/bash

# Interactive
docker start -i moringa

# Background container
docker start moringa
docker exec moringa ls -alh
docker stop moringa

# Running commands with root user (background container is the easiest way)
docker start moringa
# Example with apt update (you can't use &&, one docker exec is required for each command)
docker exec --user root moringa apt-get update
docker exec --user root moringa apt-get upgrade -y

# Useful container-specific commands, especially for background containers
docker inspect moringa         # See full container info dump
docker logs moringa            # See command logs and outputs
docker stats moringa           # Real time container statistics
docker {pause,unpause} moringa # Freeze container

# Don't forget to kill a background container when you're done
docker stop moringa

# Remove a persistent container
docker rm moringa
```

### Build images
```bash
git clone -b develop https://gitlab.irstea.fr/raffaele.gaetano/moringa.git
cd moringa

docker login gitlab-registry.irstea.fr

# OTB base image, with default USE_SYSTEM_DEPS=false (build all dependencies from source)
docker build -f docker/Dockerfile.base \
    -t gitlab-registry.irstea.fr/raffaele.gaetano/moringa/base:focal-otb72-superbuild .
docker push gitlab-registry.irstea.fr/raffaele.gaetano/moringa/base:focal-otb72-superbuild

# Tag 'moringa/base:latest' == default tag and default base image for the MORINGA build
docker tag gitlab-registry.irstea.fr/raffaele.gaetano/moringa/base:focal-otb72-superbuild \
    gitlab-registry.irstea.fr/raffaele.gaetano/moringa/base:latest
# Default pulled or pushed tag is "latest" (if not specified)
docker push gitlab-registry.irstea.fr/raffaele.gaetano/moringa/base

# MORINGA build from the default 'moringa/base' image
# You don't need to rebuild OTB if you just want to update moringa files using current branch / local commits
#git checkout custom_branch
docker build -f docker/Dockerfile.moringa -t gitlab-registry.irstea.fr/raffaele.gaetano/moringa .
docker push gitlab-registry.irstea.fr/raffaele.gaetano/moringa:latest
# Keep tracks using tags
docker tag gitlab-registry.irstea.fr/raffaele.gaetano/moringa:latest gitlab-registry.irstea.fr/raffaele.gaetano/moringa:ubuntu20
docker push gitlab-registry.irstea.fr/raffaele.gaetano/moringa:ubuntu20

# Build base image using system deps (libs installed from apt repository)
docker build -f docker/Dockerfile.base \
    --build-arg USE_SYSTEM_DEPS=true --build-arg DEPS_INSTALL_PREFIX=/usr \
    -t gitlab-registry.irstea.fr/raffaele.gaetano/moringa/base:focal-otb72-sysdeps .
docker push gitlab-registry.irstea.fr/raffaele.gaetano/moringa/base:focal-otb72-sysdeps

# Build with custom ubuntu base image
docker build --build-arg BASE_IMG=ubuntu:18.04 -f docker/Dockerfile.base \
    -t gitlab-registry.irstea.fr/raffaele.gaetano/moringa/base:bionic-otb72-superbuild .

docker build -f docker/Dockerfile.moringa --build-arg BASE_IMG=gitlab-registry.irstea.fr/raffaele.gaetano/moringa/base:bionic-otb72-superbuild \
    -t gitlab-registry.irstea.fr/raffaele.gaetano/moringa:ubuntu18 .
```

### Fix volume ownership issue (required if host's UID > 1000)
When mounting a volume, you may experience errors while trying to write files from within the container.  
Since the default user (ubuntu) is UID 1000, you won't be able to write files into your volume  
which is mounted with the same UID than your linux host user (may be UID 1001 or more).  
In order to address this, you need to edit the container's user UID and GID to match the right numerical value.
This will only persist in a named container, it is required every time you're creating a new one.


```bash
# Create a named container (here with your HOME as volume), Docker will automatically pull image
docker create --interactive --tty --volume /home/$USER:/home/ubuntu/data \
    --name moringa gitlab-registry.irstea.fr/raffaele.gaetano/moringa /bin/bash
# Start a background container process (in order to exec root commands, because default user isn't sudoer)
docker start moringa

# Exec required commands with user root (here with host's ID, replace $UID and $GID with desired values)
docker exec --user root moringa usermod ubuntu -u $UID
docker exec --user root moringa groupmod ubuntu -g $GID

# Force reset ownership with updated UID and GID. Make sure to double check that
docker exec moringa id
# Because recursive chown will apply to your volume in /home/ubuntu/data
docker exec --user root moringa chown -R ubuntu:ubuntu /home/ubuntu

# Stop the background container and start a new interactive shell
docker stop moringa
docker start -i moringa
```
```bash
# Check if ownership is right
id
ls -Alh /home/ubuntu
ls -Alh /home/ubuntu/data
# Test writing a file
touch /home/ubuntu/data/test.txt
```
