from mtdUtils import randomword
from shutil import rmtree
import getopt
import os
import platform
import sys
import subprocess
import gdal
from mtdUtils import bandToArray

def getThreshold(img):
    ds = gdal.Open(img)
    bnd = ds.GetRasterBand(1)
    arr = sorted(bandToArray(bnd).flatten())
    ds = None
    N = len(arr)
    mni = int(2*N/100)
    mxi = N - mni
    th =  (arr[mxi]-arr[mni])/4

    return th


def treeIndex(img,red_band,nir_band,radius=2,tau=0.005):
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")
    wd = os.path.dirname(img)
    td = wd + '/' + randomword(16)
    os.mkdir(td)

    # Rescaled NDVI (0-1000)
    cmd = ['otbcli_BandMath', '-il', img, '-exp', '5000*(1+(im1b' + str(nir_band) + '-im1b' + str(red_band) + ')/(im1b' + str(nir_band) + '+im1b' + str(red_band) + '))', '-out', td + '/tmp_ndvi.tif', 'uint16']
    subprocess.call(cmd,shell=sh)
    th = getThreshold(td+'/tmp_ndvi.tif')
    print('Threshold = ' + str(th))
    # Morphological opening
    cmd = ['otbcli_GrayScaleMorphologicalOperation', '-in', td + '/tmp_ndvi.tif', '-structype', 'ball', '-structype.ball.xradius', str(radius), '-structype.ball.yradius', str(radius), '-filter', 'opening', '-out', td + '/tmp_ndvi_open.tif', 'uint16']
    subprocess.call(cmd, shell=sh)
    # White top-hat
    cmd = ['otbcli_BandMath', '-il', td + '/tmp_ndvi.tif', td + '/tmp_ndvi_open.tif', '-exp', 'im1b1-im2b1', '-out', td + '/tmp_ndvi_wth.tif', 'uint16']
    subprocess.call(cmd, shell=sh)
    # Soft thresholding (reverse sigmoid fitting)
    ofn = os.path.splitext(img)[0] + '_treeindex.tif'
    cmd = ['otbcli_BandMath', '-il', td + '/tmp_ndvi_wth.tif', '-exp', '1-(1/(1+exp(-' + str(tau) + '*(im1b1-' + str(th) + '))))', '-out', ofn]
    subprocess.call(cmd, shell=sh)

    rmtree(td)

    return th

def main(argv):
    try:
        opts, args = getopt.getopt(argv, '', ['red=', 'nir=', 'radius=', 'tau='])
    except getopt.GetoptError as err:
        print(str(err))

    red = None
    nir = None
    radius = 2
    tau = 0.005

    img = args[0]
    for opt, val in opts:
        if opt == '--red':
            red = int(val)
        elif opt == '--nir':
            nir = int(val)
        elif opt == '--radius':
            radius = int(val)
        elif opt == '--tau':
            tau = float(val)

    treeIndex(img,red,nir,radius,tau)
    return 0


if __name__ == '__main__':
    if len(sys.argv) < 6:
        sys.exit(
            'Usage: python wthTreeIndex.py --red <source red channel> --nir <source nir channel> [--radius <opening radius> def. 2] [--tau <sigmoid slope> def. 0.05] <source image>')
    else:
        main(sys.argv[1:])