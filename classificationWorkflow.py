import ogr
import sys
import subprocess
import platform
import numpy as np
import os
import csv

from mtdUtils import queuedProcess, cloneVectorDataStructure, fieldToArray, mergeShapefiles

def getFeaturesFields(shp,flds_pref):

    ds = ogr.Open(shp, 0)
    ly = ds.GetLayer(0)

    flds = []
    ldfn = ly.GetLayerDefn()
    for n in range(ldfn.GetFieldCount()):
        fn = ldfn.GetFieldDefn(n).name
        if fn.startswith(tuple(flds_pref)):
            flds.append(fn)

    ds = None

    return flds

def roughFix(shp,flds):

    ds = ogr.Open(shp,1)
    ly = ds.GetLayer(0)

    arr = np.empty([ly.GetFeatureCount(), len(flds)])
    i = 0
    for f in ly:
        j = 0
        for fld in flds:
            arr[i,j] = f.GetFieldAsDouble(fld)
            j += 1
        i += 1

    #R-like rough fix
    arr[np.where(arr==-9999.0)] = np.nan
    mns = np.tile(np.nanmean(arr,axis=0),[ly.GetFeatureCount(),1])
    arr[np.isnan(arr)] = mns[np.isnan(arr)]

    ly.ResetReading()
    i = 0
    for f in ly:
        j = 0
        for fld in flds:
            f.SetField(fld,arr[i, j])
            ly.SetFeature(f)
            j += 1
        i += 1

    ds = None

    return

def baseTrainingCmd(shp,stat_file,code,flds,params,model_file,confmat_file):

    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")
    if type(shp) == str:
        shp = [shp]

    if platform.system() == 'Linux':
        cmd = ['otbcli_TrainVectorClassifier', '-io.vd'] + shp + ['-io.stats', stat_file, '-io.confmatout', confmat_file, '-cfield', code, '-io.out', model_file, '-feat'] + flds + params
        subprocess.call(cmd,shell=sh)
    elif platform.system() == 'Windows':
        import otbApplication
        app = otbApplication.Registry.CreateApplication('TrainVectorClassifier')
        app.SetParameterStringList('io.vd', shp)
        app.SetParameterString('io.stats', stat_file)
        app.SetParameterString('io.confmatout', confmat_file)
        app.SetParameterString('io.out', model_file)
        app.UpdateParameters()
        app.SetParameterStringList('cfield', [code])
        app.SetParameterStringList('feat', flds)
        # Parse classification parameters string
        # WARNING: works for all classifier with single value parameters
        # (surely hangs on <string list> classification parameter types - e.g. -classifier.ann.sizes)
        cl_param_keys = params[0::2]
        cl_param_vals = params[1::2]
        for prk,prv in zip(cl_param_keys,cl_param_vals):
            app.SetParameterString(prk[1:],prv)
        app.UpdateParameters()
        app.ExecuteAndWriteOutput()
    else:
        sys.exit("Platform not supported!")

def baseClassifyCmd(shp,stat_file,model_file,code,flds,out_file,compute_confidence=False):
    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    if platform.system() == 'Linux':
        cmd = ['otbcli_VectorClassifier', '-in', shp, '-instat', stat_file, '-model', model_file, '-cfield', code, '-feat'] + flds
        if out_file is not None:
            cmd += ['-out', out_file]
        if compute_confidence:
            cmd += ['-confmap', 1]
        subprocess.call(cmd,sh)
    elif platform.system() == 'Windows':
        import otbApplication
        app = otbApplication.Registry.CreateApplication('VectorClassifier')
        app.SetParameterString('in', shp)
        app.SetParameterString('instat', stat_file)
        app.SetParameterString('model', model_file)
        if out_file is not None:
            app.SetParameterString('out', out_file)
        if compute_confidence:
            app.SetParameterInt('confmap', 1)
        app.SetParameterString('cfield', code)
        app.UpdateParameters()
        app.SetParameterStringList('feat', flds)
        app.UpdateParameters()
        app.ExecuteAndWriteOutput()
    else:
        sys.exit('Platform not supported!')

def training(shp,code,model_fld,params,feat,feat_mode = 'list'):

    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    if '-classifier' in params:
        classifier = params[params.index('-classifier') + 1]
    else:
        classifier = 'libsvm'
    model_file = model_fld + '/' + classifier + '_' + code + '.model'
    confmat_file = model_fld + '/' + classifier + '_' + code + '.confmat.txt'
    stat_file = model_fld + '/GT_stats.xml'

    if feat_mode == 'prefix':
        flds = getFeaturesFields(shp, feat)
    elif feat_mode == 'list':
        flds = feat
    else:
        sys.exit('ERROR: mode ' + feat_mode + ' not valid.')

    if platform.system() == 'Linux':
        cmd = ['otbcli_ComputeVectorFeaturesStatistics','-io.vd',shp,'-io.stats',stat_file,'-feat'] + flds
        subprocess.call(cmd,shell=sh)
    elif platform.system() == 'Windows':
        import otbApplication
        app = otbApplication.Registry.CreateApplication('ComputeVectorFeaturesStatistics')
        app.SetParameterStringList('io.vd', [shp])
        app.SetParameterString('io.stats', model_fld + '/GT_stats.xml')
        app.UpdateParameters()
        app.SetParameterStringList('feat',flds)
        app.ExecuteAndWriteOutput()
    else:
        sys.exit("Platform not supported!")

    if platform.system() == 'Linux':
        cmd = ['otbcli_TrainVectorClassifier', '-io.vd', shp, '-io.stats', model_fld + '/GT_stats.xml', '-io.confmatout', confmat_file, '-cfield', code, '-io.out', model_file, '-feat'] + flds + params
        subprocess.call(cmd,shell=sh)
    elif platform.system() == 'Windows':
        import otbApplication
        app = otbApplication.Registry.CreateApplication('TrainVectorClassifier')
        app.SetParameterStringList('io.vd', [shp])
        app.SetParameterString('io.stats', model_fld + '/GT_stats.xml')
        app.SetParameterString('io.confmatout', confmat_file)
        app.SetParameterString('io.out', model_file)
        app.UpdateParameters()
        app.SetParameterStringList('cfield', [code])
        app.SetParameterStringList('feat', flds)
        # Parse classification parameters string
        # WARNING: works for all classifier with single value parameters
        # (surely hangs on <string list> classification parameter types - e.g. -classifier.ann.sizes)
        cl_param_keys = params[0::2]
        cl_param_vals = params[1::2]
        for prk,prv in zip(cl_param_keys,cl_param_vals):
            app.SetParameterString(prk[1:],prv)
        app.UpdateParameters()
        app.ExecuteAndWriteOutput()
    else:
        sys.exit("Platform not supported!")

    return stat_file, model_file

def classify(shp_list,code,stat_file,model_file,out_fld,out_ext,feat,feat_mode = 'list',Nproc=1,compute_confidence=False):

    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    if feat_mode == 'prefix':
        flds = getFeaturesFields(shp_list[0], feat)
    elif feat_mode == 'list':
        flds = feat
    else:
        sys.exit('ERROR: mode ' + feat_mode + ' not valid.')

    '''
    for shp in shp_list:
        #roughFix(shp, flds)
        if platform.system() == 'Linux':
            cmd = ['otbcli_VectorClassifier','-in',shp,'-instat',stat_file,'-model',model_file,'-out',out_file,'-cfield',code,'-feat'] + flds
            subprocess.call(cmd,shell=sh)
        elif platform.system() == 'Windows':
            import otbApplication
            app = otbApplication.Registry.CreateApplication('VectorClassifier')
            app.SetParameterString('in',shp)
            app.SetParameterString('instat', stat_file)
            app.SetParameterString('model', model_file)
            app.SetParameterString('out', out_file)
            app.SetParameterString('cfield', code)
            app.UpdateParameters()
            app.SetParameterStringList('feat',flds)
            app.UpdateParameters()
            app.ExecuteAndWriteOutput()
        else:
            sys.exit('Platform not supported!')

    return
    '''

    if platform.system() == 'Linux':
        cmd_list = []
        out_file_list = []
        for shp in shp_list:
            out_file = out_fld + '/' + os.path.basename(shp).replace('.shp', out_ext + '.shp')
            cmd = ['otbcli_VectorClassifier', '-in', shp, '-instat', stat_file, '-model', model_file, '-out', out_file,
                   '-cfield', code, '-feat'] + flds
            if compute_confidence:
                cmd += ['-confmap','1']
            cmd_list.append(cmd)
            out_file_list.append(out_file)
        queuedProcess(cmd_list,Nproc,shell=sh)
        return out_file_list
    elif platform.system() == 'Windows':
        out_file_list = []
        for shp in shp_list:
            out_file = out_fld + '/' + os.path.basename(shp).replace('.shp', out_ext + '.shp')
            import otbApplication
            app = otbApplication.Registry.CreateApplication('VectorClassifier')
            app.SetParameterString('in', shp)
            app.SetParameterString('instat', stat_file)
            app.SetParameterString('model', model_file)
            app.SetParameterString('out', out_file)
            app.SetParameterString('cfield', code)
            app.UpdateParameters()
            app.SetParameterStringList('feat', flds)
            if compute_confidence:
                app.SetParameterInt('confmap', 1)
            app.UpdateParameters()
            app.ExecuteAndWriteOutput()
            out_file_list.append(out_file)
        return out_file_list
    else:
        sys.exit('Platform not supported!')

def addField(filein, nameField, valueField, valueType=None,
             driver_name="ESRI Shapefile", fWidth=None):


    driver = ogr.GetDriverByName(driver_name)
    source = driver.Open(filein, 1)
    layer = source.GetLayer()
    layer_name = layer.GetName()
    layer_defn = layer.GetLayerDefn()
    field_names = [layer_defn.GetFieldDefn(i).GetName() for i in range(layer_defn.GetFieldCount())]
    if not valueType:
        try :
            int(valueField)
            new_field1 = ogr.FieldDefn(nameField, ogr.OFTInteger)
        except :
            new_field1 = ogr.FieldDefn(nameField, ogr.OFTString)
    elif valueType == str:
        new_field1 = ogr.FieldDefn(nameField, ogr.OFTString)
        sqlite_type = 'varchar'
    elif valueType == int:
        new_field1 = ogr.FieldDefn(nameField, ogr.OFTInteger)
        sqlite_type = 'int'
    elif valueType == float:
        new_field1 = ogr.FieldDefn(nameField, ogr.OFTFLOAT)
        sqlite_type = 'float'
    if fWidth:
        new_field1.SetWidth(fWidth)

    layer.CreateField(new_field1)
    for feat in layer:
        layer.SetFeature(feat)
        feat.SetField(nameField, valueField)
        layer.SetFeature(feat)

def splitShapefileByClasses(shp,code):
    ds = ogr.Open(shp)
    ly = ds.GetLayer(0)

    sep = {}
    for f in ly:
        cl = f.GetField(code)
        if cl not in list(sep.keys()):
            sep[cl] = []
        sep[cl].append(f)

    ds_dict = {}
    for cl in list(sep.keys()):
        fn = shp.replace('.shp','_' + code + '_' + str(cl) + '.shp')
        ds_dict[cl] = fn
        dsi = cloneVectorDataStructure(ds,fn)
        lyi = dsi.GetLayer(0)
        for f in sep[cl]:
            lyi.CreateFeature(f)
        dsi = None

    ds = None

    return ds_dict

def retrieveClassHierarchy(shp,code_list):
    nomen = []
    for code in code_list:
        nomen.append(fieldToArray(shp,code).astype(int))

    ds_dict = splitShapefileByClasses(shp, code_list[-1])

    h_dict = {}
    h_dict[code_list[0]] = ([0],[[ds_dict[x] for x in list(np.unique(nomen[-1]))]])
    for i in range(1,len(code_list)):
        cls = np.unique(nomen[i - 1])
        h_dict[code_list[i]] = (list(cls),[])
        for cl in cls:
            clist = list(np.unique(nomen[-1][np.where(nomen[i - 1] == cl)[0]]))
            h_dict[code_list[i]][1].append([ds_dict[x] for x in clist])

    return h_dict,list(ds_dict.values())

def Htraining(shp,code_list,model_fld,params,feat,feat_mode = 'list'):
    '''
    Hierarchical classification
    ---------------------------
    Input
    -----
    shp: str
        path of the training shapefile
    code_list: list
        list of the fields names for the different classification levels
    model_fld: str
        path to the folder where model files will be saved
    params: list
        list of parameter model to use with otbVectorTraining application
    feat: list or str
        features to use for classification
    feat_mode: list/prefix
        selection mode of the features
    ----------------------------------------------
    Output
    ------
    stat_file: str
        path to a stat_file to use to unskew model
    h_model_fld: str
        path to the folder where model files are
    '''
    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    if '-classifier' in params:
        classifier = params[params.index('-classifier') + 1]
    else:
        classifier = 'libsvm'
    h_model_fld = model_fld + '/' + 'H-MODEL_' + '_'.join(code_list)
    if not os.path.exists(h_model_fld):
        os.mkdir(h_model_fld)
    stat_file = model_fld + '/GT_stats.xml'

    if feat_mode == 'prefix':
        flds = getFeaturesFields(shp, feat)
    elif feat_mode == 'list':
        flds = feat
    else:
        sys.exit('ERROR: mode ' + feat_mode + ' not valid.')

    # Statistics unskew
    if platform.system() == 'Linux':
        cmd = ['otbcli_ComputeVectorFeaturesStatistics','-io.vd',shp,'-io.stats',stat_file,'-feat'] + flds
        subprocess.call(cmd,shell=sh)
    elif platform.system() == 'Windows':
        import otbApplication
        app = otbApplication.Registry.CreateApplication('ComputeVectorFeaturesStatistics')
        app.SetParameterStringList('io.vd', [shp])
        app.SetParameterString('io.stats', stat_file)
        app.UpdateParameters()
        app.SetParameterStringList('feat',flds)
        app.ExecuteAndWriteOutput()
    else:
        sys.exit("Platform not supported!")

    # Computes classes hierarchy in a dictionnary
    h_dict, ds_list = retrieveClassHierarchy(shp,code_list)
    # Create models for each level
    with open(h_model_fld + '/h-model.csv',mode='w') as h_model_file:
        writer = csv.writer(h_model_file)
        level = 'ROOT'
        last = list(h_dict.keys())[-1]
        for code in list(h_dict.keys()):
            Nsb = len(h_dict[code][0])
            for i in range(Nsb):
                model_file = h_model_fld + '/' + classifier + '_' + code + '_' + str(h_dict[code][0][i]) + '.model'
                confmat_file = h_model_fld + '/' + classifier + '_' + code + '_' + str(h_dict[code][0][i]) + '.confmat.txt'
                all_labels = np.concatenate([fieldToArray(x,code).astype(int) for x in h_dict[code][1][i]])
                classes_list = [str(x) for x in np.unique(all_labels)]
                baseTrainingCmd(h_dict[code][1][i],stat_file,code,feat,params,model_file,confmat_file)
                writer.writerow([level,str(h_dict[code][0][i]),model_file,code,str(code!=last),':'.join(classes_list)])
            level = code

    drv = ogr.GetDriverByName('ESRI Shapefile')
    for fn in ds_list:
        drv.DeleteDataSource(fn)

    return stat_file, h_model_fld

def Hclassify(shp_list,stat_file,h_model_fld,feat,out_fld,out_ext,feat_mode = 'list'):
    '''
    Hierarchical classification
    ---------------------------
    Input
    -----
    shp_list: list
        list of shapefile path to classify
    stat_file: str
        path to a stat_file to use to unskew model
    h_model_fld: str
        path to the folder where model files are
    feat: list or str
        features to use for classification
    out_fld: str
        path to the output folder
    out_ext: str
        output suffix
    feat_mode: list/prefix
        selection mode of the features
    ----------------------------------------------
    Output
    ------
    out_file_list : list
        list of the output shapefile paths
    '''
    if feat_mode == 'prefix':
        flds = getFeaturesFields(shp_list[0], feat)
    elif feat_mode == 'list':
        flds = feat
    else:
        sys.exit('ERROR: mode ' + feat_mode + ' not valid.')

    if not os.path.exists(h_model_fld):
        sys.exit('Folder ' + h_model_fld + ' not exists!')

    out_file_list = []
    for shp in shp_list:
        with open(h_model_fld + '/h-model.csv', mode='r') as h_model_file:
            toProcess = []
            toDelete = []
            toMerge = []
            rdr = csv.reader(h_model_file)
            for row in rdr:
                if row[0] == 'ROOT':
                    in_shp = shp
                elif len(toProcess) > 0:
                    for i,process in enumerate(toProcess):
                        if process.endswith('_p' + row[0] + '_' + row[1] + '.shp') :
                            in_shp = toProcess.pop(i)
                else:
                    continue
                if os.path.exists(in_shp):
                    out_shp = in_shp.replace('.shp','_ROOT.shp') if row[0] == 'ROOT' else None
                    split_shp = out_shp if row[0] == 'ROOT' else in_shp
                    # Classification attempt with only one class raised an error,
                    # if so, apply the value class in consequence
                    class_list = row[5].split(':')
                    #with open(row[2]) as model:
                    #    lines = model.readlines()
                    #    to_classify = int(lines[1].split(' ')[0]) != 1
                    to_classify = len(class_list) > 1
                    if to_classify :
                        baseClassifyCmd(in_shp,stat_file,row[2],'p'+row[3],flds,out_shp)
                    else :
                        #addField(in_shp,'p'+row[3],int(lines[1].split(' ')[1]))
                        addField(in_shp, 'p' + row[3], int(class_list[0]))
                    if out_shp is not None:
                        toDelete.append(out_shp)
                    #if there is a more detailed level than the current one, split the current 
                    #classification by classes to use as bases for next level 
                    if row[4] == 'True':
                        ds_dict = splitShapefileByClasses(split_shp,'p'+row[3])
                        [toProcess.insert(0,x) for x in list(ds_dict.values())]
                        toDelete.extend(list(ds_dict.values()))
                    #last level of hierarchy classes, to merge
                    elif row[4] == 'False':
                        toMerge.append(in_shp)
        # Merge all resulting shapefiles, or rename if there is only one
        if len(toMerge) > 1 :
            out_file = out_fld + '/' + os.path.basename(shp).replace('.shp', out_ext + '.shp')
            mergeShapefiles(toMerge,out_file)
            out_file_list.append(out_file)
        elif len(toMerge) == 1 :
            out_file = out_fld + '/' + os.path.basename(shp).replace('.shp', out_ext + '.shp')
            os.rename(toMerge[0],out_file)
            out_file_list.append(out_file)
        drv = ogr.GetDriverByName('ESRI Shapefile')
        # Drop tmp files
        for fn in toDelete:
           drv.DeleteDataSource(fn)

    return out_file_list