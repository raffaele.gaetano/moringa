import getopt
import os
import subprocess
import sys
import glob
import gdal
import osr
import shutil
import platform
import stat
import numpy as np

from mtdUtils import randomword, getRasterExtentAsShapefile

def genScript(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:c:t:',['srs='])
    except getopt.GetoptError as err:
        print(str(err))

    # Get sub-folders names
    dirs = [os.path.join(args[0], dn) for dn in os.listdir(args[0])]
    # Parse options
    od = os.getcwd()
    clipping = False
    clip_shp = None
    single_tile = ''
    srs = None
    for opt, val in opts:
        if opt == '-o':
            od = val
        elif opt == '-c':
            clip_shp = val
            clipping = True
        elif opt == '-t':
            single_tile = val
        elif opt == '--srs':
            srs = str(val)
    if srs is None :
        ds = gdal.Open(glob.glob(os.path.join(dirs[0],'*_FRE.DBL.TIF'))[0])
        prj = ds.GetProjectionRef()
        srs = osr.SpatialReference(prj)
        srs = 'EPSG:'+srs.GetAuthorityCode('PROJCS')

    if platform.system() == 'Windows':
        f = open('VENUSPreparation.bat', 'w')
        f.write("@echo off\n")
    else:
        f = open('VENUSPreparation.sh', 'w')

    for dir in dirs:
        if single_tile != '' and single_tile not in dir:
            continue
        else:
            mtd = glob.glob(os.path.join(dir, '*_L2VALD_*.HDR'))
            if len(mtd) > 0:
                #cmd = ['python', 'prepareVENUS.py'] + argv[:-1] + [dir]
                cmd = ['python3', 'prepareVENUS.py', '-o',  od ]
                if clip_shp is not None:
                    cmd += ['-c', clip_shp ]
                cmd += ['--srs', srs]
                cmd += [dir]
                f.write(' '.join(cmd) + '\n')
    f.close()

    if platform.system() == 'Linux':
        st = os.stat('VENUSPreparation.sh')
        os.chmod('VENUSPreparation.sh', st.st_mode | stat.S_IEXEC)

    return

def prepare(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:c:', ['srs='])
    except getopt.GetoptError as err:
        print(str(err))

    # Parse options
    od = os.getcwd()
    clipping = False
    clip_shp = None
    srs = None

    for opt, val in opts:
        if opt == '-o':
            od = val
        elif opt == '-c':
            clip_shp = val
            clipping = True
        elif opt == '--srs':
            srs = str(val)

    src_dir = args[0]
    tile_date = src_dir.split('_')[-1].split('.')[0]
    tile_id = '_'.join(src_dir.split('_')[:-1]).split('L2VALD_')[-1].replace('_','-')

    if os.environ.get('GDAL_DATA') == None:
        sys.exit('Please set GDAL_DATA environment variable!')

    print(('Processing ' + src_dir + '...'))

    # Create output directories
    print(od)
    if not os.path.exists(od):
        os.mkdir(od)
    seqnum = 0
    pd = od
    od = pd + '/VENUS-L2A-THEIA_' + str(seqnum) + '_' + tile_date + '_' + tile_id
    while os.path.exists(od):
        seqnum += 1
        od = pd + '/VENUS-L2A-THEIA_' + str(seqnum) + '_' + tile_date + '_' + tile_id
    os.mkdir(od)

    curdir = os.getcwd()
    os.chdir(od)

    # Prepare ROI (full extent if not clipping)
    tmpname = randomword(16)
    if not clipping:
        clip_shp = od + '/' + tmpname + '.shp'
        ext_ref = glob.glob(os.path.join(src_dir, '*_FRE.DBL.TIF'))[0]
        getRasterExtentAsShapefile(ext_ref, clip_shp)
        ext = 'full'
    else:
        ext = os.path.splitext(os.path.basename(clip_shp))[0]

    clip_shp_val = clip_shp
    # Generate and run clip commands
    clip_cmd = 'gdalwarp -q -of GTiff -ot Int16 -dstnodata -10000 -srcnodata -10000 -cutline \"' + clip_shp_val + '\" -crop_to_cutline '
    if srs != None :
        clip_cmd += '-t_srs {} '.format(srs)

    fn = glob.glob(os.path.join(src_dir, '*_FRE.DBL.TIF'))[0]
    out_fn = od + '/' + os.path.basename(fn).split('.')[0] + '_' + ext + '.tif'
    subprocess.call(clip_cmd + '\"' + fn + '\" \"' + out_fn + '\"', shell=True)

    clip_cmd = 'gdalwarp -q -of GTiff -ot UInt16 -dstnodata 0 -srcnodata 0 -cutline \"' + clip_shp_val + '\" -crop_to_cutline '
    if srs != None :
        clip_cmd += '-t_srs {} '.format(srs)
    fn = glob.glob(os.path.join(src_dir, '*_CLD.DBL.TIF'))[0]
    out_tmp_fn = od + '/' + os.path.basename(fn).split('.')[0] + '_tmp.tif'
    cmd = ['otbcli_BandMath', '-il', fn, '-exp', 'im1b1+1', '-out', out_tmp_fn, 'uint16']
    subprocess.call(' '.join(cmd), shell=True)
    out_fn = od + '/' + os.path.basename(fn).split('.')[0] + '_' + ext + '.tif'
    subprocess.call(clip_cmd + '\"' + out_tmp_fn + '\" \"' + out_fn + '\"', shell=True)
    os.remove(out_tmp_fn)

    mtd_fn = glob.glob(os.path.join(src_dir, os.path.basename(src_dir).split('.')[0]+'.HDR'))[0]
    shutil.copyfile(mtd_fn, od + '/' + os.path.basename(mtd_fn))

    ds = gdal.Open(glob.glob(os.path.join(od,'*FRE*.tif'))[0])
    ly = ds.GetRasterBand(1)
    nbval = len(np.unique(ly.ReadAsArray()))
    ds = None
    if nbval == 1 :
        print((" {}_{} doesn't cover requested area, deleting folder".format(tile_date,tile_id)))
        shutil.rmtree(od)

    os.chdir(curdir)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python prepareVENUS.py [-o <output dir>] [-c <clip_shp>] [-t tile] <original THEIA tile folder / parent folder>\n'
            'If parent folder is given, searches for all available images and creates a preparation batch file.')
    else:
        if len(glob.glob(os.path.join(sys.argv[-1],'*.HDR'))) > 0 :
            dirs = [os.path.join(sys.argv[-1],dir) for dir in os.listdir(sys.argv[-1]) if os.path.isdir(os.path.join(sys.argv[-1],dir))]
            for dir_name in dirs:
                hdr_file = dir_name.split('.')[0] + '.HDR'
                print(hdr_file)
                os.rename(hdr_file,os.path.join(dir_name, os.path.basename(hdr_file)))
        mtd = glob.glob(os.path.join(sys.argv[-1], '*_L2VALD_*.HDR'))
        if len(mtd) > 0:
            prepare(sys.argv[1:])
        else:
            genScript(sys.argv[1:])
