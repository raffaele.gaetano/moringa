import getopt
import os
import subprocess
import sys

from mtdUtils import randomword, dateYMDtoYJ


def getLandsat8_AmazonAWS_byurl(pageurl, od):
    # Create output directory
    os.mkdir(od)
    url_list = randomword(8) + '.txt'
    f = open(url_list, 'w')
    fnsb = ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'BQA']
    imgid = pageurl.split('/')[-2]
    fns = [imgid + '_' + x for x in fnsb]
    out_fns = []
    for fn in fns:
        f.write(pageurl + fn + '.TIF' + '\n')
        out_fns.append(od + '/' + fn + '.TIF')
    f.write(pageurl + imgid + '_MTL.txt' + '\n')
    f.close()
    # call WGET
    subprocess.call('wget -q -i ' + url_list + ' -P \"' + od + '\" --no-check-certificate', shell=True)
    os.remove(url_list)
    return out_fns

def getLandsat8_AmazonAWS(date, pathrow, od, coll='pre', ing_date=''):
    # Create output directory
    os.mkdir(od)
    # Parse date "yyyymmdd"
    dj = dateYMDtoYJ(date)
    # Parse pathrow ex. "159074"
    path = pathrow[0:3]
    row = pathrow[3:6]
    # create WGET command
    if coll=="pre":
        imgid = 'LC8' + pathrow + dj + 'LGN00'
        baseaws = 'http://landsat-pds.s3.amazonaws.com/L8/'
    elif coll=="c1" and ing_date!='':
        imgid = 'LC08_L1TP_' + pathrow + '_' + date + '_' + ing_date + '_01_T1'
        baseaws = 'http://landsat-pds.s3.amazonaws.com/c1/L8/'
    else:
        sys.exit("Bad collection or missing ingestion date for Collection 1")

    page = '/'.join([path, row, imgid])
    baseurl = baseaws + page + '/'

    return getLandsat8_AmazonAWS_byurl(baseurl,od)


def readL8Metadata(mdf):
    # Open MTL
    mtl = open(mdf, 'r')
    # Prepare output
    out = {}
    # For each info, read and store
    for line in mtl:
        val = line.strip().split(' = ')
        if 'CLOUD_COVER' in val[0]:
            out['cloudyPixelPercentage'] = (float(val[1]))
    # Return dictionary
    return out


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:c:rtu:', ['url='])
    except getopt.GetoptError as err:
        print(str(err))

    # Parse options
    od = os.getcwd()
    clipping = False
    clip_shp = None
    removeOriginal = False
    convTOA = False
    byURL = False

    for opt, val in opts:
        if opt == '-o':
            od = val
        elif opt == '-c':
            clip_shp = val
            clipping = True
        elif opt == '-r':
            removeOriginal = True
        elif opt == '-t':
            convTOA = True
        elif opt in ['-u','--url']:
            byURL = True
            url = val

    if os.environ.get('GDAL_DATA') == None:
        sys.exit('Please set GDAL_DATA environment variable!')

    # Create output directory
    if not os.path.exists(od):
        os.mkdir(od)
    od = od + '/L8_' + args[0] + '_' + args[1]
    if os.path.exists(od):
        print('Output directory already exists!')
    else:
        # DOWNLOAD COMMAND
        print(('Downloading ' + od + '...'))
        if byURL:
            out_fns = getLandsat8_AmazonAWS_byurl(url, od)
        else:
            out_fns = getLandsat8_AmazonAWS(args[0], args[1], od)

    if clipping:
        curdir = os.getcwd()
        os.chdir(od)
        ext = os.path.splitext(os.path.basename(clip_shp))[0]
        print("Clipping...")
        clip_shp_val = clip_shp
        clip_cmd = 'gdalwarp -q -of GTiff -ot UInt16 -dstnodata 0 -srcnodata 0 -cutline \"' + clip_shp_val + '\" -crop_to_cutline '
        '''
        imgid = 'LC8' + args[1] + dateYMDtoYJ(args[0]) + 'LGN00'
        fnsb = ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'BQA']
        fns = [imgid + '_' + x for x in fnsb]
        '''
        fns = sorted([os.path.splitext(os.path.basename(out_fns[i]))[0] for i in range(len(out_fns)) if out_fns[i].endswith('.TIF')])
        imgid = fns[0][0:-3]
        out_fns = []
        for fn in fns:
            out_fn = od + '/' + fn + '_' + ext + '.TIF'
            out_fns.append(out_fn)
            subprocess.call(clip_cmd + '\"' + od + '/' + fn + '.TIF\" \"' + out_fn + '\"', shell=True)
        os.rename(od + '/' + imgid + '_MTL.txt', od + '/' + imgid + '_MTL_' + ext + '.txt')
        files = os.listdir(os.getcwd())
        if removeOriginal:
            for file in fns:
                os.remove(os.path.join(os.getcwd(), file + '.TIF'))
        os.chdir(curdir)

    '''
    if convTOA:
        print "Converting to TOA/BTemp..."
        os.mkdir(od + '/TOA')
        if clipping:
            L8_TOAReflectance(od + '/' + imgid + '_MTL_' + ext + '.txt', od + '/TOA/')
        else:
            L8_TOAReflectance(od + '/' + imgid + '_MTL.txt', od + '/TOA/')
    '''


if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.exit(
            'Usage: python getLandsat8.py [-o <output dir>] [-c <clip_shp>] [-r] [--url <aws_url>] <date "yyyymmdd"> <pathrow "xxxyyy">')
    else:
        main(sys.argv[1:])
