import glob
import os
import platform
import shutil
import subprocess
import sys
import getopt


def maja2theia(infld,outfld):
    if platform.system() == "Linux":
        sh = False
    elif platform.system() == "Windows":
        sh = True
    else:
        sys.exit('Platform ' + platform.system() + 'not supported.')

    basename = 'SENTINEL2' + os.path.basename(infld)[2] + '_'
    ref = glob.glob(infld + '/*FRE_R1*.TIF')[0]
    date = os.path.basename(ref).split('____')[1].split('_')[0]
    tile = 'T' + os.path.basename(ref).split('____')[0].split('_')[-1]
    niv = 'L2A_'
    # niv et tile
    basename += date + '_' + niv + tile + '_D_V1-3'

    if not os.path.exists(outfld):
        os.mkdir(outfld)

    outtilefld = outfld + os.sep + tile
    if not os.path.exists(outtilefld):
        os.mkdir(outtilefld)

    outfld = outtilefld + os.sep + basename
    if not os.path.exists(outfld):
        os.mkdir(outfld)

    cmd = ['otbcli_SplitImage', '-in', ref, '-out', outfld + '/' + basename + '_FRE_B.tif', 'int16']
    subprocess.call(cmd, shell=sh)
    for oex, nex in zip(['B_0', 'B_1', 'B_2', 'B_3'], ['B2', 'B3', 'B4', 'B8']):
        of = outfld + '/' + basename + '_FRE_' + oex + '.tif'
        nf = outfld + '/' + basename + '_FRE_' + nex + '.tif'
        os.rename(of, nf)

    cmd = ['otbcli_SplitImage', '-in', ref.replace('_R1', '_R2'), '-out', outfld + '/' + basename + '_FRE_B.tif',
           'int16']
    subprocess.call(cmd, shell=sh)
    for oex, nex in zip(['B_0', 'B_1', 'B_2', 'B_3', 'B_4', 'B_5'], ['B5', 'B6', 'B7', 'B8A', 'B11', 'B12']):
        of = outfld + '/' + basename + '_FRE_' + oex + '.tif'
        nf = outfld + '/' + basename + '_FRE_' + nex + '.tif'
        os.rename(of, nf)

    if not os.path.exists(outfld + '/MASKS'):
        os.mkdir(outfld + '/MASKS')

    sat = outfld + '/MASKS/' + basename + '_SAT_R1.tif'
    edg = outfld + '/MASKS/' + basename + '_EDG_R1.tif'
    clmin1 = (ref.replace('PDTIMG', 'PDTANX')).replace('FRE_R1', 'CLD_R1')
    clmin2 = (ref.replace('PDTIMG', 'PDTANX')).replace('FRE_R1', 'CLD_R2')
    clmout1 = outfld + '/MASKS/' + basename + '_CLM_R1.tif'
    clmout2 = outfld + '/MASKS/' + basename + '_CLM_R2.tif'

    cmd = ['otbcli_BandMath', '-il', ref, '-out', sat, 'uint8', '-exp', '(0*im1b1)']
    subprocess.call(cmd, shell=sh)
    cmd = ['otbcli_BandMath', '-il', ref, '-out', edg, 'uint8', '-exp', '(im1b1==-10000)']
    subprocess.call(cmd, shell=sh)

    shutil.copyfile(clmin1, clmout1)
    shutil.copyfile(clmin2, clmout2)

    mtd = open(outfld + os.sep + basename + '_MTD_ALL.xml','w')
    mtd.write('*** Dummy metadata file for MAJA outcome to match THEIA format ***\n')
    mtd.close()

    print('Reformat complete!')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python MAJA2THEIAFormat.py [-o <output-dir>] [-r] <input image folder / parent folder>')
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'o:r')
    except getopt.GetoptError as err:
        sys.exit(err)

    ofld = os.getcwd()
    removing = False

    for opt, val in opts:
        if opt == '-o':
            ofld = val
        if opt == '-r':
            removing = True

    lst = glob.glob(args[0] + os.sep + 'S2*_SSC*L2VALD_*.*')
    fldlst = [fld for fld in lst if fld.endswith('.DIR')]
    if len(fldlst) > 0 :
        for fld in fldlst:
            maja2theia(fld,ofld)
            if removing:
                shutil.rmtree(fld)
    elif len(lst) > 0 :
        print(args[0].rstrip(os.sep))
        maja2theia(args[0].rstrip(os.sep), ofld)
        if removing:
            shutil.rmtree(args[0])
    else:
        print("No MAJA products found in folder " + args[0])
