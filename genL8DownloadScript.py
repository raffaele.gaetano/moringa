import datetime
import getopt
import os
import platform
import stat
import subprocess
import sys

import ogr
import osr
import gzip
import csv

from getLandsat8 import readL8Metadata
from mtdUtils import dateYMDtoYJ, randomword


def findpathrows(shp):
    wrs2 = ogr.Open('./aux_data/wrs2_descending.shp')
    clip = ogr.Open(shp)
    wrs2_ly = wrs2.GetLayer(0)
    clip_ly = clip.GetLayer(0)

    pathrows = []
    ovl = []

    wrs2_srs = wrs2_ly.GetSpatialRef()
    clip_srs = clip_ly.GetSpatialRef()
    tr = osr.CoordinateTransformation(clip_srs, wrs2_srs)

    for fr in wrs2_ly:
        clip_ly.ResetReading()
        gr = fr.GetGeometryRef()
        for f in clip_ly:
            g = f.GetGeometryRef()
            g.Transform(tr)
            if g.Intersects(gr):
                intsect = g.Intersection(gr)
                pr = str(int(fr.GetField('PR'))).zfill(6)
                if pr not in pathrows:
                    pathrows.append(pr)
                    ovl.append(intsect.GetArea() / g.GetArea())
                else:
                    ovl[pathrows.index(pr)] += intsect.GetArea() / g.GetArea()

    idx = sorted(list(range(len(ovl))), key=lambda k: ovl[k], reverse=True)
    tot = 0.0
    i = 0
    while i < len(ovl) and tot < 1.0:
        tot += ovl[idx[i]]
        i += 1

    final_pathrows = [pathrows[k] for k in idx[0:i]]

    return final_pathrows


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:crm:', ['max-clouds='])
    except getopt.GetoptError as err:
        sys.exit(err)

    maxcc = 100.0

    # tile
    if os.path.exists(args[0]):
        pathrows = findpathrows(args[0])
    else:
        pathrows = args[0].split(':')

    # Parse options
    opt_str = ''
    for opt, val in opts:
        if opt == '-o':
            opt_str = opt_str + ' -o \"' + val + '\"'
        elif opt == '-c' and os.path.exists(args[0]):
            opt_str = opt_str + ' -c \"' + args[0] + '\"'
        elif opt == '-r':
            opt_str = opt_str + ' -r'
        elif opt == '-t':
            opt_str = opt_str + ' -t'
        elif opt == '--max-clouds':
            maxcc = float(val)

    sh = False

    if platform.system() == 'Windows':
        f = open('L8DownloadScript.bat', 'w')
        f.write("@echo off\n")
        sh = True
    else:
        f = open('L8DownloadScript.sh', 'w')

    g = open('L8SearchLog.txt', 'w')

    sd = args[1][0:4] + '-' + args[1][4:6] + '-' + args[1][6:8]
    ed = args[2][0:4] + '-' + args[2][4:6] + '-' + args[2][6:8]

    #Collection 1
    scenelist = './' + randomword(16) + '.gz'
    cmd = ['wget','-q','-O',scenelist,'http://landsat-pds.s3.amazonaws.com/c1/L8/scene_list.gz']
    subprocess.call(cmd,shell=sh)
    sl = gzip.open(scenelist)
    rdr = csv.reader(sl)
    frdr = [p for p in rdr if p[0][-2:] == 'T1' and p[2] >= sd and p[2] <= ed and p[4] == 'L1TP' and (str(int(p[5])).zfill(3) + str(int(p[6])).zfill(3)) in pathrows]
    found_dates = []
    for im in frdr:
        dt = im[2][0:4]+im[2][5:7]+im[2][8:10]
        pathrow = (str(int(im[5])).zfill(3) + str(int(im[6])).zfill(3))
        if float(im[3]) <= maxcc and pathrow+dt not in found_dates:
            found_dates.append(pathrow+dt)
            f.write('python getLandsat8.py' + opt_str + ' ' + '--url ' + os.path.dirname(im[-1]) + '/ ' + dt + ' ' + pathrow + '\n')
            g.write('Adding    date ' + dt + ' for path/row ' + pathrow[0:3] + '/' + pathrow[3:6] + ' with ' + str(float(im[3])) + '% cloudy pixels (Collection 1)\n')
        else:
            g.write('Rejecting date ' + dt + ' for path/row ' + pathrow[0:3] + '/' + pathrow[3:6] + ' with ' + str(
                float(im[3])) + '% cloudy pixels (Collection 1)\n')
    sl.close()
    os.remove(scenelist)

    # Pre-collection
    scenelist = './' + randomword(16) + '.gz'
    cmd = ['wget', '-q', '-O', scenelist, 'http://landsat-pds.s3.amazonaws.com/scene_list.gz']
    subprocess.call(cmd, shell=sh)
    sl = gzip.open(scenelist)
    rdr = csv.reader(sl)
    frdr = [p for p in rdr if p[1] >= sd and p[1] <= ed and (str(int(p[4])).zfill(3) + str(
        int(p[5])).zfill(3)) in pathrows]
    for im in frdr:
        dt = im[1][0:4] + im[1][5:7] + im[1][8:10]
        pathrow = (str(int(im[4])).zfill(3) + str(int(im[5])).zfill(3))
        if float(im[2]) <= maxcc and pathrow+dt not in found_dates:
            found_dates.append(pathrow+dt)
            f.write('python getLandsat8.py' + opt_str + ' ' + '--url ' + os.path.dirname(
                im[-1]) + '/ ' + dt + ' ' + pathrow + '\n')
            g.write('Adding    date ' + dt + ' for path/row ' + pathrow[0:3] + '/' + pathrow[3:6] + ' with ' + str(
                float(im[2])) + '% cloudy pixels (Pre-collection)\n')
        else:
            g.write('Rejecting date ' + dt + ' for path/row ' + pathrow[0:3] + '/' + pathrow[3:6] + ' with ' + str(
                float(im[2])) + '% cloudy pixels (Pre-collection)\n')
    sl.close()
    os.remove(scenelist)

    if len(found_dates) > 0:
        print("Products have been found!")

    f.close()
    g.close()


    """
    for pathrow in pathrows:
        path = pathrow[0:3]
        row = pathrow[3:6]
        # date and date range
        sd = datetime.date(int(args[1][0:4]), int(args[1][4:6]), int(args[1][6:8]))
        ed = datetime.date(int(args[2][0:4]), int(args[2][4:6]), int(args[2][6:8])) + datetime.timedelta(1)
        cd = sd
        valid_dates = []
        print 'Scanning date interval for path ' + path + ', row ' + row + '...'
        while cd != ed:
            cdstr = cd.strftime('%Y%m%d')
            dj = dateYMDtoYJ(cdstr)
            imgid = 'LC8' + pathrow + dj + 'LGN00'
            page = '/'.join([path, row, imgid])
            url = 'http://landsat-pds.s3.amazonaws.com/L8/' + page + '/' + imgid + '_MTL.txt'
            ret = subprocess.call('wget -q ' + url, shell=True)
            if ret == 0:
                info = readL8Metadata(imgid + '_MTL.txt')
                if info['cloudyPixelPercentage'] <= maxcc:
                    g.write('Adding    date ' + cdstr + ' for path/row ' + path + '/' + row + ' with ' + str(
                        info['cloudyPixelPercentage']) + '% cloudy pixels\n')
                    valid_dates.append(cdstr)
                else:
                    g.write('Rejecting date ' + cdstr + ' for path/row ' + path + '/' + row + ' with ' + str(
                        info['cloudyPixelPercentage']) + '% cloudy pixels\n')
                os.remove(imgid + '_MTL.txt')
            cd += datetime.timedelta(1)
        if len(valid_dates) > 0:
            print "Products have been found!"
            for dt in valid_dates:
                f.write('python getLandsat8.py ' + opt_str + ' ' + dt + ' ' + pathrow + '\n')
    f.close()
    """


    if platform.system() == 'Linux':
        st = os.stat('L8DownloadScript.sh')
        os.chmod('L8DownloadScript.sh', st.st_mode | stat.S_IEXEC)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python genL8DownloadScript.py [-o <output-dir>] [-c] [-r] [--max-clouds <perc>] <shapefile OR colon separated pathrow numbers> <start date> <end date>')
    main(sys.argv[1:])
