import getopt
import glob
import os
import platform
import subprocess
import sys
import gdal
import otbApplication as otb
from mtdUtils import setNoDataValue, periodic_date_generator

gdt_to_otb = {2 : 'uint16', 3 : 'int16', 6 : 'float'}

def computeNViews(fld, ofld = None, name = None):
    if ofld == None:
        ofld = fld
    ofile = 'NVIEWS.tif?gdal:co:compress=deflate'
    if name is not None:
        ofile = name + '_' + ofile
    lst = sorted(glob.glob(fld + '/*/*MASK*.tif'))
    expr = '+'.join(['im%db1' % (i+1) for i in range(len(lst))])
    bm = otb.Registry.CreateApplication('BandMath')
    bm.SetParameterStringList('il',lst)
    bm.SetParameterString('exp', expr)
    bm.SetParameterString('out', ofld + os.sep + ofile)
    bm.SetParameterOutputImagePixelType('out', otb.ImagePixelType_uint8)
    bm.ExecuteAndWriteOutput()
    return

def gatherDates(fld,ofld='.'):
    lst = sorted([name for name in os.listdir(fld) if (os.path.isdir(os.path.join(fld, name)) and (name[0:2] in ['S2','L8','VE']))], key = lambda x: x.split('_')[-2])
    ofn = ofld + '/input_dates.txt'
    sns = set()
    with open(ofn,'w') as fl:
        for l in lst:
            fl.write(l.split('_')[-2] + '\n')
            sns.add(l[0:2])
    if len(sns) != 1 or list(sns)[0] not in ['S2','L8','VE']:
        sys.exit('Error: directory contains images from multiple sensors or invalid detected folders.')
    return lst,list(sns)

def rebuildSeries(stack_fn, nc, od_fn, out_fld):
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    with open(od_fn) as ofn:
        dt = ofn.read().splitlines()

    i = 0
    nm = os.path.splitext(os.path.basename(stack_fn))[0].split('_')
    if not os.path.exists(out_fld):
        os.mkdir(out_fld)
    ds = gdal.Open(stack_fn)
    dty = gdt_to_otb[ds.GetRasterBand(1).DataType]
    dty = commonPixTypeToOTB(dty)
    ds = None

    for d in dt:
        onm = nm[0] + '_' + d + '_' + nm[2]
        if not os.path.exists(out_fld + '/' + onm):
            os.mkdir(out_fld + '/' + onm)
        else:
            sys.exit('Folder ' + out_fld + '/' + onm + ' already exists!')
        expr = '{' + ','.join(['im1b'+str(b) for b in range(i+1,i+nc+1)]) + '}'

        bandMathX = otb.Registry.CreateApplication('BandMathX')
        if bandMathX is None:
            raise Exception("Not possible to create 'BandMathX' application, \
                         check if OTB is well configured / installed")

        bandMathX.SetParameterStringList("il", [stack_fn])
        bandMathX.SetParameterString("exp", expr)
        bandMathX.SetParameterString("out", out_fld + '/' + onm + '/STACK_gapf.tif')
        bandMathX.SetParameterOutputImagePixelType("out",dty)
        bandMathX.ExecuteAndWriteOutput()

        # cmd = ['otbcli_BandMathX','-il',stack_fn,'-exp',expr,'-out',out_fld + '/' + onm + '/STACK_gapf.tif',dty]
        # subprocess.call(cmd, shell=sh)

        setting_nodata = otb.Registry.CreateApplication('ManageNoData')
        if setting_nodata is None:
            raise Exception("Not possible to create 'ManageNoData' application, \
                         check if OTB is well configured / installed")

        setting_nodata.SetParameterString("in", out_fld + '/' + onm + '/STACK_gapf.tif')
        setting_nodata.SetParameterString("out", out_fld + '/' + onm + '/MASK_gapf.tif')
        setting_nodata.SetParameterString("mode","buildmask")
        setting_nodata.SetParameterString("mode.buildmask.inv",'1')
        setting_nodata.SetParameterOutputImagePixelType("out",commonPixTypeToOTB('uint8'))
        setting_nodata.ExecuteAndWriteOutput()

        # cmd = ['otbcli_ManageNoData', '-in', out_fld + '/' + onm + '/STACK_gapf.tif', '-mode', 'buildmask', '-out', out_fld + '/' + onm + '/MASK_gapf.tif', 'uint8']
        # subprocess.call(cmd, shell=sh)

        bandMath = otb.Registry.CreateApplication('BandMathX')
        if bandMath is None:
            raise Exception("Not possible to create 'BandMathX' application, \
                         check if OTB is well configured / installed")

        bandMath.SetParameterStringList("il", [out_fld + '/' + onm + '/MASK_gapf.tif'])
        bandMath.SetParameterString("exp", '{im1b1==2}')
        bandMath.SetParameterString("out", out_fld + '/' + onm + '/GAPMASK_gapf.tif')
        bandMath.SetParameterOutputImagePixelType("out",commonPixTypeToOTB('uint8'))
        bandMath.ExecuteAndWriteOutput()

        # cmd = ['otbcli_BandMath', '-il', out_fld + '/' + onm + '/MASK_gapf.tif', '-exp', 'im1b1==2', '-out', out_fld + '/' + onm + '/GAPMASK_gapf.tif', 'uint8']
        # subprocess.call(cmd, shell=sh)

        # Dummy metadata file generation
        if nm[0] == 'S2':
            fn = 'tileInfo.json'
        elif nm[0] == 'S2-L1C-PEPS':
            fn = 'metadata.xml'
        elif nm[0] == 'S2-L2A':
            fn = 'MTD_TL_MSIL2A.xml'
        elif nm[0] == 'S2-L2A-THEIA':
            fn = 'GAPF_MTD_ALL.xml'
        elif nm[0] == 'L8':
            fn = 'GAPF_MTL.txt'
        elif nm[0] == 'VENUS-L2A-THEIA':
            fn = 'GAPF_L2VALD.HDR'
        with open(out_fld + '/' + onm + '/' + fn,'w') as mf:
            mf.write('*** Dummy metadata file for ' + nm[0] + ' ***')


        i += nc


def commonPixTypeToOTB(string):
    dico = {"complexDouble": otb.ComplexImagePixelType_double,
            "complexFloat": otb.ComplexImagePixelType_float,
            "double": otb.ImagePixelType_double,
            "float": otb.ImagePixelType_float,
            "int16": otb.ImagePixelType_int16,
            "int32": otb.ImagePixelType_int32,
            "uint16": otb.ImagePixelType_uint16,
            "uint32": otb.ImagePixelType_uint32,
            "uint8": otb.ImagePixelType_uint8}
    try:
        return dico[string]
    except:
        raise Exception("Error in commonPixTypeToOTB function input parameter : " + string + " not available, choices are :"
                        "'complexDouble','complexFloat','double','float','int16','int32','uint16','uint32','uint8'")

def getInputParameterOutput(otbObj):

    """
    IN :
    otbObj [otb object]

    OUT :
    output parameter name

    Ex :
    otbBandMath = otb.CreateBandMathApplication(...)
    print getInputParameterOutput(otbBandMath)
    >> out

    /!\ this function is not complete, it must be fill up...
    """
    listParam = otbObj.GetParametersKeys()
    #check out
    if "out" in listParam:
        return "out"
    #check io.out
    elif "io.out" in listParam:
        return "io.out"
    #check mode.raster.out
    elif "mode.raster.out" in listParam:
        return "mode.raster.out"
    elif "outputstack" in listParam:
        return "outputstack"
    else:
        raise Exception("out parameter not recognize")

def genComposite(fld, od_fn, ofld = '.', onExtent = None):
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")
    lst,sns = gatherDates(fld,ofld)
    ptrn = '*STACK_*.tif'
    mptrn = '*MASK_*.tif'
    if sns[0] == 'L8':
        ptrn = '*_PS_TOA*.TIF'
        mptrn = '*MASK_HR*.TIF'

    stacklist = []
    gaplist = []
    gapcmdlist = []
    nc = set()
    dt = set()

    # Generate envelope gap masks
    env_fn = ofld + os.sep + 'composite_envelope.tif'
    refl = glob.glob(fld + os.sep + lst[0] + os.sep + ptrn)[0]
    if onExtent is not None and os.path.exists(onExtent):
        rasterization = otb.Registry.CreateApplication('Rasterization')
        if rasterization is None:
            raise Exception("Not possible to create 'Rasterization' application, \
                         check if OTB is well configured / installed")

        rasterization.SetParameterString("in", onExtent)
        rasterization.SetParameterString("im", refl)
        rasterization.SetParameterString("mode.binary.foreground", 'l')
        rasterization.SetParameterString("out", env_fn)
        rasterization.SetParameterOutputImagePixelType("out",commonPixTypeToOTB('uint8'))
        rasterization.ExecuteAndWriteOutput()
        # cmd = ['otbcli_Rasterization', '-in', onExtent, '-im', refl, '-out', env_fn, 'uint8',
        #        '-mode.binary.foreground', '1']
    else:
        rasterization = otb.Registry.CreateApplication('BandMathX')
        if rasterization is None:
            raise Exception("Not possible to create 'BandMathX' application, \
                         check if OTB is well configured / installed")

        rasterization.SetParameterStringList("il", [refl])
        rasterization.SetParameterString("exp", "{im1b1>=0}")
        rasterization.SetParameterString("out", env_fn)
        rasterization.SetParameterOutputImagePixelType("out",commonPixTypeToOTB('uint8'))
        rasterization.ExecuteAndWriteOutput()
    #     cmd = ['otbcli_BandMathX', '-il', refl, '-exp', 'im1b1>=0', '-out', env_fn, 'uint8']
    # subprocess.call(cmd, shell=sh)

    # Build output name
    ds = gdal.Open(env_fn)
    gt_ref = ds.GetGeoTransform()
    ds = None
    tile_list = []
    for l in lst:
        tile = l.split('_')[-1]
        if tile not in tile_list:
            tile_list.append(tile)
    nm = lst[0].split('_')
    if len(tile_list) > 1 :
        ext = "MOSAIC"
    else :
        ext = nm[-1]
    if onExtent is not None and os.path.exists(onExtent):
        ext = os.path.splitext(os.path.basename(onExtent))[0].replace('_','-')
    onm = nm[0] + '_SERIES_' + ext + '.tif'
    onmg = nm[0] + '_SERIES-GAPF_' + ext + '.tif'

    for l in lst:
        fn = glob.glob(fld + '/' + l + '/' + ptrn)[0]
        msk_fn = glob.glob(fld + '/' + l + '/' + mptrn)[0]
        ds = gdal.Open(fn)
        gt = ds.GetGeoTransform()
        nc.add(ds.RasterCount)
        dt.add(ds.GetRasterBand(1).DataType)
        ds = None

        if gt != gt_ref:
            tmp_fn = fn.replace('.','_tmp.')
            os.rename(fn,tmp_fn)
            SIApp = otb.Registry.CreateApplication('Superimpose')
            SIApp.SetParameterString("inr",env_fn)
            SIApp.SetParameterString("inm",tmp_fn)
            SIApp.SetParameterString("out",fn)
            SIApp.SetParameterOutputImagePixelType("out",commonPixTypeToOTB('uint16'))
            SIApp.ExecuteAndWriteOutput()
            os.remove(tmp_fn)
            tmp_msk_fn = msk_fn.replace('.','_tmp.')
            os.rename(msk_fn, tmp_msk_fn)
            SIApp = otb.Registry.CreateApplication('Superimpose')
            SIApp.SetParameterString("inr",env_fn)
            SIApp.SetParameterString("inm",tmp_msk_fn)
            SIApp.SetParameterString("out",msk_fn)
            SIApp.SetParameterOutputImagePixelType("out",commonPixTypeToOTB('uint8'))
            SIApp.ExecuteAndWriteOutput()
            os.remove(tmp_msk_fn)

        stacklist.append(fn)
        gap_fn = ofld + os.sep + l + '_GAPMASK.tif'
        gapmask_calc = otb.Registry.CreateApplication('BandMathX')
        if gapmask_calc is None:
            raise Exception("Not possible to create 'BandMathX' application, \
                         check if OTB is well configured / installed")

        gapmask_calc.SetParameterStringList("il", [env_fn, msk_fn])
        gapmask_calc.SetParameterString("exp", '{im1b1 != 0 && im2b1 == 0}')
        gapmask_calc.SetParameterString("out", gap_fn)
        gapmask_calc.SetParameterOutputImagePixelType("out",commonPixTypeToOTB('uint8'))
        gapmask_calc.ExecuteAndWriteOutput()

        setNoDataValue(gap_fn, None)
        # gap_cmd = ['otbcli_BandMathX', '-il', env_fn, msk_fn, '-exp', '{im1b1 != 0 && im2b1 == 0}', '-out', gap_fn, 'uint8']
        gaplist.append(gap_fn)
        # gapcmdlist.append(gap_cmd)

    if len(nc) != 1:
        sys.exit('Error: stacks to composite do not have the same number of bands.')
    if len(dt) != 1:
        sys.exit('Error: stacks to composite do not have the same data format.')

    dtout = gdt_to_otb[list(dt)[0]]
    dtout = commonPixTypeToOTB(dtout)
    ncomp = str(list(nc)[0])

    nodataval = 0
    if list(dt)[0] > 2:
        nodataval = -10000
    
    concatenate_images = otb.Registry.CreateApplication('ConcatenateImages')
    if concatenate_images is None:
        raise Exception("Not possible to create 'ConcatenateImages' application, \
                     check if OTB is well configured / installed")

    concatenate_images.SetParameterStringList("il", stacklist)
    concatenate_images.SetParameterString("out", ofld + '/serie_temp.tif')
    concatenate_images.SetParameterOutputImagePixelType("out",dtout)
    concatenate_images.Execute()

    # setting_nodata = otb.Registry.CreateApplication('ManageNoData')
    # if setting_nodata is None:
    #     raise Exception("Not possible to create 'ManageNoData' application, \
    #                  check if OTB is well configured / installed")

    concatenate_images_gap = otb.Registry.CreateApplication('ConcatenateImages')
    if concatenate_images_gap is None:
        raise Exception("Not possible to create 'ConcatenateImages' application, \
                     check if OTB is well configured / installed")

    concatenate_images_gap.SetParameterStringList("il", gaplist)
    concatenate_images_gap.SetParameterString("out", ofld + '/GAPMASKS.tif')
    concatenate_images_gap.SetParameterOutputImagePixelType("out",commonPixTypeToOTB('uint8'))
    concatenate_images_gap.Execute()

    # cmd = ['otbcli_ConcatenateImages', '-il'] + gaplist + ['-out', ofld + '/GAPMASKS.tif', 'uint8']
    # subprocess.call(cmd, shell=sh)

    gapfilling = otb.Registry.CreateApplication("ImageTimeSeriesGapFilling")
    if concatenate_images_gap is None:
        raise Exception("Not possible to create 'ImageTimeSeriesGapFilling' application, \
                     check if OTB is well configured / installed")
    gapfilling.SetParameterInputImage("in", concatenate_images.GetParameterOutputImage(getInputParameterOutput(concatenate_images)))
    gapfilling.SetParameterInputImage("mask", concatenate_images_gap.GetParameterOutputImage(getInputParameterOutput(concatenate_images_gap)))
    gapfilling.SetParameterString("comp", ncomp)
    gapfilling.SetParameterString("it", "linear")
    gapfilling.SetParameterString("id", ofld + '/input_dates.txt')
    gapfilling.SetParameterString("od", od_fn)
    gapfilling.SetParameterString("out", ofld + '/serie_gapf_temp.tif')
    gapfilling.SetParameterOutputImagePixelType("out", dtout)
    gapfilling.Execute()

    setting_nodata_gapf = otb.Registry.CreateApplication('ManageNoData')
    if setting_nodata_gapf is None:
        raise Exception("Not possible to create 'ManageNoData' application, \
                     check if OTB is well configured / installed")

    setting_nodata_gapf.SetParameterInputImage("in", gapfilling.GetParameterOutputImage(getInputParameterOutput(gapfilling)))
    setting_nodata_gapf.SetParameterString("out", ofld + '/' + onmg)
    setting_nodata_gapf.SetParameterString("mode","changevalue")
    setting_nodata_gapf.SetParameterString("mode.changevalue.newv", str(nodataval))
    setting_nodata_gapf.SetParameterOutputImagePixelType("out",dtout)
    setting_nodata_gapf.ExecuteAndWriteOutput()
    # cmd = ['otbcli_ImageTimeSeriesGapFilling', '-in', ofld + '/' + onm, '-mask', ofld + '/GAPMASKS.tif', '-out', ofld + '/' + onmg, dtout, '-comp', ncomp, '-it', 'linear', '-id', ofld + '/input_dates.txt', '-od', od_fn]
    # subprocess.call(cmd, shell=sh)
    setNoDataValue(ofld + '/' + onmg, nodataval)

    return ofld + os.sep + onmg, ncomp

def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:d:')
    except getopt.GetoptError as err:
        sys.exit(err)

    fld = args[-1]
    
    output_folder = os.path.join(fld,'GAPF')
    #if not os.path.exists(output_folder):
    #    os.mkdir(output_folder)
    date_file = None
    for opt,val in opts:
        if opt == '-o':
            output_folder = str(val)
        elif opt == '-d':
            date_file = str(val)

    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    
    if date_file == None :
        _,_= gatherDates(fld,output_folder)
        with open(os.path.join(output_folder,'input_dates.txt')) as ofn:
            dts = ofn.read().splitlines()
        dts = sorted(list(set(dts)))
        date_file = os.path.join(output_folder,'output_dates.txt')
        with open(date_file,'w') as fl:
            for dt in dts:
                fl.write(dt + '\n')
    else:
        if date_file.isdigit() is True:
            lst, _ = gatherDates(fld, output_folder)
            lst = [l.split('_')[-2] for l in lst]
            date_step = int(date_file)
            date_beg = lst[0][0:4] + '-' + lst[0][4:6] + '-' + lst[0][6:8]
            date_end = lst[-1][0:4] + '-' + lst[-1][4:6] + '-' + lst[-1][6:8]
            date_file = os.path.join(output_folder,'output_dates.txt')
            periodic_date_generator(date_file,date_beg,date_end,date_step)
        else:
            if not os.path.exists(date_file):
                sys.exit("\nWARNING : -o argument must be an existant file of dates or an integer to generate the dates file\n")

    composite_stack, ncomp = genComposite(fld, date_file, output_folder)
    rebuildSeries(composite_stack, int(ncomp), date_file, output_folder)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python cloudfreeComposites.py [-o <output folder>] [-d <output dates file>] <source-folder>')
    else:
        main(sys.argv[1:])