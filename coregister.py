import getopt
import glob
import os
import platform
import subprocess
import sys
import warnings
from shutil import rmtree, copyfile

import gdal
import osr

from mtdUtils import homoGeo2Pixel, randomword, compareImageGeometries


def main(argv):
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    try:
        opts, args = getopt.getopt(argv, '', ['band-ref=', 'band-in=', 'step=', 'minstep=', 'minpoints=', 'maxpoints=', 'prec='])
    except getopt.GetoptError as err:
        print(str(err))

    ref = args[0]
    src = args[1]

    ds = gdal.Open(args[1])
    prj = ds.GetProjection()
    gt = ds.GetGeoTransform()
    srs = osr.SpatialReference()
    srs.ImportFromWkt(prj)
    code = srs.GetAuthorityCode(None)
    gsp = str(int(2 * round(max(abs(gt[1]), abs(gt[5])))))
    ds = None

    vhrb = 3
    hrb = 3

    # Internal calibration.. To adapt?
    step = 256
    minstep = 16
    enough = 20
    prec = 3
    maxgcp = -1

    for opt, val in opts:
        if opt == '--band-ref':
            vhrb = val
        elif opt == '--band-in':
            hrb = val
        elif opt == '--step':
            step = int(val)
        elif opt == '--minstep':
            minstep = int(val)
        elif opt == '--minpoints':
            enough = int(val)
        elif opt == '--maxpoints':
            maxgcp = int(val)
        elif opt == '--prec':
            prec = float(val)

    tmphomo = randomword(16) + '.geom'
    tmpfld = os.path.split(args[1])[0] + '/' + randomword(16) + '/'
    if not os.path.exists(tmpfld):
        os.mkdir(tmpfld)
    else:
        sys.exit('Oups, temp directory exists!')
    ext = os.path.splitext(args[1])[1]

    if not compareImageGeometries(args[0],args[1]):
        refimg = tmpfld + 'ref.tif'
        cmd = ['otbcli_Superimpose', '-inr', args[1], '-inm', args[0], '-out', refimg, 'uint16']
        subprocess.call(cmd, shell=sh)
    else:
        refimg = args[0]

    # Internal calibration.. To adapt?
    npts = 0

    while npts < enough and step >= minstep:

        cmd = ['otbcli_HomologousPointsExtraction', '-in1', args[1], '-band1', str(hrb), '-in2', refimg,
               '-band2',
               str(vhrb), '-mode', 'geobins', '-precision', str(prec), '-mfilter', '1', '-backmatching', '1', '-out',
               tmpfld + 'homo.txt',
               '-2wgs84', '1', '-algorithm', 'sift', '-mode.geobins.binstep', str(step)]
        subprocess.call(cmd, shell=sh)

        i = 0
        with open(tmpfld + 'homo.txt') as homo:
            for i, l in enumerate(homo):
                pass
            npts = i + 1
        step /= 2

    if npts >= enough:

        homo_sample_rate = None
        if maxgcp > 0:
            homo_sample_rate = float(maxgcp)/float(npts)

        homoGeo2Pixel(tmpfld + 'homo.txt', args[1], tmpfld + 'homocorr.txt', sample_rate=homo_sample_rate)
        cmd = ['otbcli_GenerateRPCSensorModel', '-inpoints', tmpfld + 'homocorr.txt', '-outgeom', tmphomo, '-map', 'epsg', 'map.epsg.code', '4326']
        subprocess.call(cmd, shell=sh)

        # Remove Cartographic Information from source (temporary)
        ds = gdal.Open(args[1],1)
        geoT = ds.GetGeoTransform()
        proj = ds.GetProjection()
        ds.SetGeoTransform(tuple([0.0,1.0,0.0,0.0,0.0,1.0]))
        ds.SetProjection('')
        ds = None

        tmp_code = code
        # Workaround for southern hemisphere images with NORTH projection
        if tmp_code.startswith('326') and gt[3] < 0:
            warnings.warn("Original image is in southern hemisphere with north UTM projection. Using south projection for temporary output.")
            tmp_code = tmp_code[0:2] + '7' + tmp_code[3:]
        cmd = ['otbcli_OrthoRectification', '-io.in \"' + args[1] + '?&geom=' + tmphomo + '\"', '-io.out', tmpfld + 'sits_band_coreg.tif', 'uint16', '-map', 'epsg', '-map.epsg.code', tmp_code, '-opt.gridspacing', gsp]
        cmdex = ' '.join(cmd)
        subprocess.call(cmdex, shell=True)

        # Restore Cartographic Information on source
        ds = gdal.Open(args[1], 1)
        ds.SetGeoTransform(geoT)
        ds.SetProjection(proj)
        ds = None

        cmd = ['otbcli_Superimpose', '-inr', args[1], '-inm', tmpfld + 'sits_band_coreg.tif', '-out',
               args[1].replace(ext, ext.replace('.', '_COREG.')), 'uint16']
        subprocess.call(cmd, shell=sh)

        msks = glob.glob(tmpfld + '../*MASK*.*')
        for f in msks:
            ff = os.path.split(f)[-1]
            fn = os.path.splitext(ff)
            if fn[1] in ['.tif', '.TIF']:

                # Remove Cartographic Information from source (temporary)
                ds = gdal.Open(f, 1)
                geoT = ds.GetGeoTransform()
                proj = ds.GetProjection()
                ds.SetGeoTransform(tuple([0.0, 1.0, 0.0, 0.0, 0.0, 1.0]))
                ds.SetProjection('')
                ds = None

                cmd = ['otbcli_OrthoRectification', '-io.in \"' + f + '?&geom=' + tmphomo + '\"', '-io.out', tmpfld + fn[0] + '_coreg.tif', 'uint8', '-map', 'epsg', '-map.epsg.code', tmp_code, '-opt.gridspacing', gsp, '-interpolator nn']
                cmdex = ' '.join(cmd)
                subprocess.call(cmdex, shell=True)

                # Restore Cartographic Information on source
                ds = gdal.Open(f, 1)
                ds.SetGeoTransform(geoT)
                ds.SetProjection(proj)
                ds = None

                cmd = ['otbcli_Superimpose', '-inr', args[1], '-inm', tmpfld + fn[0] + '_coreg.tif', '-out',
                       f.replace(fn[1], fn[1].replace('.', '_COREG.')), 'uint8', '-interpolator', 'nn']
                subprocess.call(cmd, shell=sh)

        copyfile(tmphomo, os.path.split(args[1])[0] + '/' + 'coregistered.geom')
        os.remove(tmphomo)

    else:

        warnings.warn("Not enough homologous points (" + str(npts) + ") for a good coregistration. Keeping source.")

    rmtree(tmpfld)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python coregister.py [--band-ref <target ref band>] [--band-in <source band>] [--step <geobins initial step def. 256>] [--minstep <geobins minimum step def. 16>] [--minpoints <minimum # of homologous points def. 40>] [--maxpoints <maximum # of GCP points def. -1 unlimited> [--prec <colocalisation precision in pixels] <vhsr scene file> <stack to coregister>')
    else:
        main(sys.argv[1:])
