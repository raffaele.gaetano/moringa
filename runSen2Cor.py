import sys
import os
import glob
import getopt
import shutil
from mtdUtils import queuedProcess

def runSen2Cor(fld, s2c_fld, nproc = 1, options = []):
    basecmd = [s2c_fld + os.sep + 'bin' + os.sep + 'L2A_Process'] + options
    lst = glob.glob(fld + os.sep + '*MSIL1C*.SAFE')
    cmdlist = [' '.join(basecmd + [l] + ['>', l.replace('.SAFE','.log')]) for l in lst]

    queuedProcess(cmdlist, nproc, shell=True, delay=1)

    lst = glob.glob(fld + os.sep + '*MSIL2A*.SAFE')
    for l in lst:
        success = len(glob.glob(l + '/GRANULE/*/IMG_DATA/*')) > 0
        if not success:
            #print('toDel: ' + l)
            shutil.rmtree(l)

    return


if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.exit(
            'Usage: python runSen2Cor.py [-n <number of parallel processes> def. 1] [-o <Sen2Cor options in a quote>] <Sen2Cor install folder> <folder containing S2 MSIL1C>\n'
            'Ex. python runSen2Cor.py -n 12 /home/ocsol_data/_tools/Sen2Cor-02.05.05-Linux64 /home/ocsol_data/Montcuq/S2')
    else:
        try:
            opts, args = getopt.getopt(sys.argv[1:], 'n:o:', [])
        except getopt.GetoptError as err:
            print(str(err))

        options = []
        nproc = 1
        for opt, val in opts:
            if opt == '-n':
                nproc = int(val)
            if opt == '-o':
                options = val.split(' ')

        runSen2Cor(args[1], args[0], nproc, options)
