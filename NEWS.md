# New version

## Add

- argument overwrite to avoid recomputation for already computed files
- argument ram to control OTB applications RAM limit
- automatic download of SRTM (package elevation) and geoid
- argument to specify cache directory where SRTM tiles are stored  

## Changes

- parameter nproc becomes ncores
- parallelization with mulitprocessing

## Fix

- intermediate files are stored in subdirectories of output directory (input directory is not changed anymore)