import getopt
import sys
from math import sin, radians
from shutil import copyfile

from mtdUtils import *


def L8_TOAReflectance(MTL, odr):
    if MTL == '' or not os.path.isfile(MTL):
        sys.exit("Please provide a valid MTL file!")

    dr = os.path.dirname(MTL) + '/'
    fn = os.path.basename(MTL).replace('txt', 'tif')
    ms_fn = [dr + fn.replace('MTL', 'B1'), \
             dr + fn.replace('MTL', 'B2'), \
             dr + fn.replace('MTL', 'B3'), \
             dr + fn.replace('MTL', 'B4'), \
             dr + fn.replace('MTL', 'B5'), \
             dr + fn.replace('MTL', 'B6'), \
             dr + fn.replace('MTL', 'B7'), \
             dr + fn.replace('MTL', 'B9'), \
             dr + fn.replace('MTL', 'B10'), \
             dr + fn.replace('MTL', 'B11')]

    pan_fn = dr + fn.replace('MTL', 'B8')

    mtl = open(MTL, 'r')
    rad_mul = []
    rad_add = []
    ref_mul = []
    ref_add = []
    theta_se = 0
    k1 = []
    k2 = []

    for line in mtl:
        val = line.strip().split(' = ')
        if 'RADIANCE_MULT_BAND_' in val[0]:
            rad_mul.append(float(val[1]))
        elif 'RADIANCE_ADD_BAND_' in val[0]:
            rad_add.append(float(val[1]))
        elif 'REFLECTANCE_MULT_BAND_' in val[0]:
            ref_mul.append(float(val[1]))
        elif 'REFLECTANCE_ADD_BAND_' in val[0]:
            ref_add.append(float(val[1]))
        elif 'SUN_ELEVATION' in val[0]:
            theta_se = float(val[1])
        elif 'K1_CONSTANT_BAND' in val[0]:
            k1.append(float(val[1]))
        elif 'K2_CONSTANT_BAND' in val[0]:
            k2.append(float(val[1]))

    mtl.close()

    ms0 = gdal.Open(ms_fn[0])
    msy, msx = ms0.RasterYSize, ms0.RasterXSize
    ms_arr = np.empty([10, ms0.RasterYSize, ms0.RasterXSize])
    msGeoT = ms0.GetGeoTransform()
    msProj = ms0.GetProjection()
    p = gdal.Open(pan_fn)
    py, px = p.RasterYSize, p.RasterXSize
    pan_arr = np.empty([p.RasterYSize, p.RasterXSize])
    pGeoT = p.GetGeoTransform()
    pProj = p.GetProjection()
    ms0 = None
    p = None

    img = gdal.Open(pan_fn)
    bnd = bandToArray(img.GetRasterBand(1))
    pan_arr = (bnd > 0) * (ref_mul[7] * bnd + ref_add[7]) / sin(radians(theta_se))

    s = 0
    for k in [0, 1, 2, 3, 4, 5, 6, 8]:
        img = gdal.Open(ms_fn[k])
        bnd = bandToArray(img.GetRasterBand(1))
        ms_arr[s] = (bnd > 0) * (ref_mul[k] * bnd + ref_add[k]) / sin(radians(theta_se))
        s = s + 1

    for k in [9, 10]:
        img = gdal.Open(ms_fn[k - 1])
        bnd = bandToArray(img.GetRasterBand(1))
        ms_arr[s] = (bnd > 0) * (k2[k - 9] / np.log((k1[k - 9] / (rad_mul[k] * bnd + rad_add[k])) + 1))
        s = s + 1

    olims_fn = odr + fn.replace('MTL', 'OLI_MS_TOARef')
    olipan_fn = odr + fn.replace('MTL', 'OLI_P_TOARef')
    tirs_fn = odr + fn.replace('MTL', 'TIRS_BTemp')

    drv = gdal.GetDriverByName('GTiff')
    olims = drv.Create(olims_fn, msx, msy, 8, gdal.GDT_UInt16)
    olims.SetGeoTransform(msGeoT)
    olims.SetProjection(msProj)
    olipan = drv.Create(olipan_fn, px, py, 1, gdal.GDT_UInt16)
    olipan.SetGeoTransform(pGeoT)
    olipan.SetProjection(pProj)
    tirs = drv.Create(tirs_fn, msx, msy, 2, gdal.GDT_Float32)
    tirs.SetGeoTransform(msGeoT)
    tirs.SetProjection(msProj)

    for k in range(0, 8):
        arrayToBand(olims.GetRasterBand(k + 1), (10000 * ms_arr[k]).astype(np.uint16))

    for k in range(0, 2):
        arrayToBand(tirs.GetRasterBand(k + 1), ms_arr[k + 8].astype(np.float32))

    arrayToBand(olipan.GetRasterBand(1), (10000 * pan_arr).astype(np.uint16))

    olims.FlushCache()
    olipan.FlushCache()
    tirs.FlushCache()

    olims = None
    olipan = None
    tirs = None

    setNoDataValue(olims_fn)
    setNoDataValue(olipan_fn)
    setNoDataValue(tirs_fn)

    bqa = os.path.basename(olims_fn).replace('OLI_MS_TOARef', 'BQA')
    copyfile(dr + bqa, odr + bqa)

    return 0


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'i:o:', ['input-folder=', 'output-folder='])
    except getopt.GetoptError as err:
        print str(err)

    in_folder = os.getcwd() + '/'
    out_folder = './'

    for opt, val in opts:
        if opt in ('-i', '--input--folder'):
            in_folder = val + "/"
        elif opt in ('-o', '--output-folder'):
            out_folder = val + "/"

    for file in os.listdir(in_folder):
        if 'MTL' in file:
            print('Converting ' + os.path.basename(file) + ' to TOA...')
            L8_TOAReflectance(in_folder + file, out_folder)


if __name__ == '__main__':
    print "Warning! Deprecated function. Cloud mask algorithm automatically provides TOA for Landsat 8 products."
    if len(sys.argv) < 3:
        sys.exit('Usage: python L8_TOARef.py -i <input dir> -o <output dir>')
    else:
        main(sys.argv[1:])
