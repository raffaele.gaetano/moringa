import getopt
import os
import subprocess
import sys


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'm:e:f:', ['mask=', 'extent=', 'output-folder='])
    except getopt.GetoptError as err:
        print str(err)

    out_folder = "./"

    fname = os.path.split(args[0])[-1]
    print 'Clipping image : ' + fname + '...'

    for opt, val in opts:
        if opt in ('-m', '--mask'):
            mfull = val
            mname = os.path.split(val)[-1]
            clip_by_mask = True
        elif opt in ('-e', '--extent'):
            clip_by_mask = False
        elif opt in ('-f', '--output-folder'):
            out_folder = val + "/"

    if clip_by_mask == True:
        foutfull = out_folder + os.path.splitext(os.path.basename(args[0]))[0] + '_' + \
                   os.path.splitext(os.path.basename(mfull))[0] + '.tif'
        subprocess.call('gdalwarp -q -of GTiff -dstnodata 0 -cutline \"' + mfull + '\" -crop_to_cutline \"' + args[
            0] + '\" \"' + foutfull + '\"')
    else:
        print 'Not implemented yet'


if __name__ == '__main__':
    main(sys.argv[1:])
