# [sudo] Modify the following variables and copy file in /etc/profile.d
export OTB_INSTALL_DIR=/opt/geospatial/otb-superbuild
export RIOS_ROOT=/opt/geospatial/rios
export FMASK_ROOT=/opt/geospatial/fmask

# Do not modify!
export PATH=$OTB_INSTALL_DIR/bin:$RIOS_ROOT/bin:$FMASK_ROOT/bin:${PATH}
export OTB_APPLICATION_PATH=$OTB_INSTALL_DIR/lib/otb/applications
export LD_LIBRARY_PATH=$OTB_INSTALL_DIR/lib:${LD_LIBRARY_PATH}
export PYTHONPATH=$OTB_INSTALL_DIR/lib/otb/python:$OTB_INSTALL_DIR/lib64/python2.7/site-packages:$PYTHONPATH
export PYTHONPATH=${PYTHONPATH}:$RIOS_ROOT/lib/python2.7/site-packages:$FMASK_ROOT/lib64/python2.7/site-packages
export GDAL_DATA=$OTB_INSTALL_DIR/share/gdal
export RIOS_DFLT_DRIVER="GTiff"

# Developer variables
export CPATH=$OTB_INSTALL_DIR/include:${C_INCLUDE_PATH}
export LIBRARY_PATH=$OTB_INSTALL_DIR/lib:${LIBRARY_PATH}
