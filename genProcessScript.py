import getopt
import glob
import os
import platform
import stat
import sys


def main(argv):
    try:
        opts, args = getopt.getopt(argv, '', ['cloudmask', 's2ref=', 's2bandlist=', 'makefile', 'compress'])
    except getopt.GetoptError as err:
        print(str(err))

    opt_str = ''
    opt_str_s2 = ''
    base_fld = args[0]
    makefile = False

    for opt, val in opts:
        if opt == '--cloudmask':
            opt_str += ' --cloudmask'
        elif opt == '--s2ref':
            opt_str_s2 += ' --s2ref ' + val
        elif opt == '--s2bandlist':
            opt_str_s2 += ' --s2bandlist ' + val
        elif opt == '--makefile':
            makefile = True
        elif opt == '--compress':
            opt_str += ' --compress'

    valid_dirs_L8 = []
    valid_dirs_S2 = []
    valid_dirs_S2L2A = []
    valid_dirs_S2_THEIA = []
    valid_dirs_Venus = []
    for d in os.walk(base_fld):
        fld = d[0]
        mdfL8 = glob.glob(fld + '/*_MTL*.txt')
        mdfS2 = glob.glob(fld + '/tileInfo.json')
        mdfS2L2A = glob.glob(fld + '/MTD_TL_MSIL2A.xml')
        mdfS2THEIA = glob.glob(fld + '/*_MTD_ALL.xml')
        mdfVenus = glob.glob(fld + '/*_L2VALD_*.HDR')
        if len(mdfL8) > 0:
            valid_dirs_L8.append(fld)
        elif len(mdfS2) > 0:
            valid_dirs_S2.append(fld)
        elif len(mdfS2L2A) > 0:
            valid_dirs_S2L2A.append(fld)
        elif len(mdfS2THEIA) > 0:
            valid_dirs_S2_THEIA.append(fld)
        elif len(mdfVenus) > 0:
            valid_dirs_Venus.append(fld)

    if (len(valid_dirs_L8) + len(valid_dirs_S2) + len(valid_dirs_S2L2A) + len(valid_dirs_S2_THEIA) + len(valid_dirs_Venus)) == 0:
        sys.exit('No products found within ' + base_fld)

    if not makefile:
        if platform.system() == 'Windows':
            f = open('ProcessScript.bat', 'w')
            f.write("@echo off\n")
        else:
            f = open('ProcessScript.sh', 'w')

        for d in valid_dirs_L8:
            f.write('python3 preprocess.py ' + opt_str + ' ' + d + '\n')
        for d in valid_dirs_S2:
            f.write('python3 preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n')
        for d in valid_dirs_S2L2A:
            f.write('python3 preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n')
        for d in valid_dirs_S2_THEIA:
            f.write('python3 preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n')
        for d in valid_dirs_Venus:
            f.write('python3 preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n')
        f.close()

        if platform.system() == 'Linux':
            st = os.stat('ProcessScript.sh')
            os.chmod('ProcessScript.sh', st.st_mode | stat.S_IEXEC)

    else:
        f = open('ProcessMakefile', 'w')
        tot = len(valid_dirs_L8) + len(valid_dirs_S2) + len(valid_dirs_S2_THEIA) + len(valid_dirs_Venus)
        joblist = ['job%d' % j for j in range(1,tot+1)]
        joball = ' '.join(joblist)
        f.write('all : ' + joball + '\n\n')
        for d in valid_dirs_L8:
            f.write(joblist.pop(0) + ':\n')
            f.write('\tpython3 preprocess.py ' + opt_str + ' ' + d + '\n\n')
        for d in valid_dirs_S2:
            f.write(joblist.pop(0) + ':\n')
            f.write('\tpython3 preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n\n')
        for d in valid_dirs_S2L2A:
            f.write(joblist.pop(0) + ':\n')
            f.write('\tpython3 preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n\n')
        for d in valid_dirs_S2_THEIA:
            f.write(joblist.pop(0) + ':\n')
            f.write('\tpython3 preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n\n')
        for d in valid_dirs_Venus:
            f.write(joblist.pop(0) + ':\n')
            f.write('\tpython3 preprocess.py ' + opt_str + ' ' + opt_str_s2 + ' ' + d + '\n\n')
        f.close()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python genProcessScript.py [--cloudmask] [--s2ref <ref_band>] [--s2bandlist <colon separated band list>] [--compress] <images-dir>')
    main(sys.argv[1:])
