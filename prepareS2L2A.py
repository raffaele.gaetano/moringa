import getopt
import os
import subprocess
import sys
import glob
import shutil
import gdal
import osr
import platform
import stat
import zipfile
import numpy as np

from mtdUtils import randomword, getRasterExtentAsShapefile


def genScript(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:c:t:', ['srs=','compress','tif'])
    except getopt.GetoptError as err:
        print(str(err))

    # Get sub-folders names
    dirs = [args[0] + os.sep + dn for dn in os.listdir(args[0]) if os.path.isdir(args[0] + os.sep + dn) and 'MSIL2A' in dn]

    # Parse options
    od = os.getcwd()
    clip_shp = None
    single_tile = ''
    srs = None
    compress = False
    iext = '.jp2'

    for opt, val in opts:
        if opt == '-o':
            od = val
        elif opt == '-c':
            clip_shp = val
        elif opt == '-t':
            single_tile = val
        elif opt == '--srs':
            srs = str(val)
        elif opt == '--compress':
            compress = True
        elif opt == '--tif':
            iext = '.tif'
    if srs is None:
        ds = gdal.Open(glob.glob(dirs[0] + '/GRANULE/*/IMG_DATA/R10m/*_B02_10m' + iext)[0])
        prj = ds.GetProjectionRef()
        srs = osr.SpatialReference(prj)
        srs = 'EPSG:' + srs.GetAuthorityCode('PROJCS')

    if platform.system() == 'Windows':
        f = open('S2L2APreparation.bat', 'w')
        f.write("@echo off\n")
    else:
        f = open('S2L2APreparation.sh', 'w')

    for dir in dirs:
        if single_tile != '' and single_tile not in dir:
            continue
        else:
            mtd = glob.glob(dir + '/GRANULE/*/IMG_DATA/R10m/*B02_10m' + iext)
            if len(mtd) > 0:
                cmd = ['python3', 'prepareS2L2A.py', '-o', od]
                if clip_shp is not None:
                    cmd += ['-c', clip_shp]
                cmd += ['--srs', srs]
                if compress:
                    cmd += ['--compress']
                if iext == '.tif':
                    cmd += ['--tif']
                cmd += [dir]
                f.write(' '.join(cmd) + '\n')
    f.close()

    if platform.system() == 'Linux':
        st = os.stat('S2L2APreparation.sh')
        os.chmod('S2L2APreparation.sh', st.st_mode | stat.S_IEXEC)

    return


def prepare(argv):
    try:
        opts, args = getopt.getopt(argv, 'o:c:', ['srs=','compress', 'tif'])
    except getopt.GetoptError as err:
        print(str(err))

    # Parse options
    od = os.getcwd()
    clipping = False
    clip_shp = None
    srs = None
    compress = False
    iext = '.jp2'

    for opt, val in opts:
        if opt == '-o':
            od = val
        elif opt == '-c':
            clip_shp = val
            clipping = True
        elif opt == '--srs':
            srs = str(val)
        elif opt == '--compress':
            compress = True
        elif opt == '--tif':
            iext = '.tif'

    src_dir = args[0]
    tile_date = os.path.basename(src_dir)[11:19]
    tile_id = os.path.basename(src_dir)[39:44]

    if os.environ.get('GDAL_DATA') == None:
        sys.exit('Please set GDAL_DATA environment variable!')

    print(('Processing ' + src_dir + '...'))

    # Create output directories
    if not os.path.exists(od):
        os.mkdir(od)
    seqnum = 0
    pd = od
    od = pd + '/S2-L2A_' + str(seqnum) + '_' + tile_date + '_' + tile_id
    while os.path.exists(od):
        seqnum += 1
        od = pd + '/S2-L2A_' + str(seqnum) + '_' + tile_date + '_' + tile_id
    os.mkdir(od)

    curdir = os.getcwd()
    os.chdir(od)

    #with open(od + '/tileInfo.json', 'w') as mf:
    #    mf.write('*** Dummy metadata file for S2-L2A_' + str(seqnum) + '_' + tile_date + '_' + tile_id + ' ***')

    # Prepare ROI (full extent if not clipping)
    tmpname = randomword(16)
    if not clipping:
        clip_shp = od + '/' + tmpname + '.shp'
        ext_ref = glob.glob(src_dir + '/GRANULE/*/IMG_DATA/R10m/*_B02_10m' + iext)[0]
        getRasterExtentAsShapefile(ext_ref, clip_shp)
        ext = 'full'
    else:
        ext = os.path.splitext(os.path.basename(clip_shp))[0]

    clip_shp_val = clip_shp
    # Generate and run clip commands
    clip_cmd = 'gdalwarp -q -of GTiff -ot Int16 -dstnodata -10000 -srcnodata 0 -cutline {} -crop_to_cutline '.format(
        clip_shp_val)
    if srs != None:
        clip_cmd += '-t_srs {} '.format(srs)
    if compress:
        clip_cmd += '-co \"COMPRESS=DEFLATE\" '
    fns = ['R10m/*_B02_10m', 'R10m/*_B03_10m', 'R10m/*_B04_10m', 'R20m/*_B05_20m', 'R20m/*_B06_20m', 'R20m/*_B07_20m', 'R10m/*_B08_10m', 'R20m/*_B8A_20m', 'R20m/*_B11_20m', 'R20m/*_B12_20m']

    for fn_pre in fns:
        fn = glob.glob(src_dir + '/GRANULE/*/IMG_DATA/' + fn_pre + iext)[0]
        out_fn = od + '/' + os.path.splitext(os.path.basename(fn))[0][:-4] + '_' + ext + '.tif'
        subprocess.call(clip_cmd + '\"' + fn + '\" \"' + out_fn + '\"', shell=True)

    clip_cmd = 'gdalwarp -q -of GTiff -ot Byte -cutline \"' + clip_shp_val + '\" -crop_to_cutline '
    if srs != None:
        clip_cmd += '-t_srs {} '.format(srs)
    if compress:
        clip_cmd += '-co \"COMPRESS=DEFLATE\" '
    fn = glob.glob(src_dir + '/GRANULE/*/IMG_DATA/R20m/*_SCL_20m' + iext)[0]
    ref = glob.glob(od + '/*B04*.tif')[0]
    out_fn = od + '/' + os.path.splitext(os.path.basename(fn))[0][:-4] + '_' + ext + '.tif'
    if compress:
        out_fn += '?gdal:co:compress=deflate'
    cmd = ['otbcli_Superimpose', '-inm', fn, '-inr', ref, '-interpolator', 'nn', '-out', out_fn, 'uint8']
    subprocess.call(' '.join(cmd), shell=True)

    #mtd_fn = glob.glob(src_dir + '/GRANULE/*/MTD_TL.xml')[0]
    #shutil.copyfile(mtd_fn, od + '/' + 'MTD_TL_MSIL2A.xml')
    with open(od + '/' + 'MTD_TL_MSIL2A.xml', 'w') as mtd:
        mtd.write("*** DUMMY METADATA FILE FOR S2-L2A ***")
        mtd.close()

    # check if the tile are in the wanted area or not
    ds = gdal.Open(glob.glob(os.path.join(od, '*B04*.tif'))[0])
    ly = ds.GetRasterBand(1)
    nbval = len(np.unique(ly.ReadAsArray()))
    ds = None
    if nbval == 1:
        print((" {}_{} doesn't cover requested area, deleting folder".format(tile_date, tile_id)))
        shutil.rmtree(od)

    os.chdir(curdir)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.exit(
            'Usage: python prepareS2L2A.py [-o <output dir>] [-c <clip_shp>] [--srs <EPSG:4326] [-t tile] <original L2A tile folder / parent folder>\n'
            'If parent folder is given, searches for all available images and creates a preparation batch file.')
    else:
        #mtd = glob.glob(sys.argv[-1] + '/*MTD_MSIL2A.xml')
        mtd = glob.glob(sys.argv[-1] + '/GRANULE/*/IMG_DATA/R10m/*B02_10m.*')
        if len(mtd) > 0:
            prepare(sys.argv[1:])
        else:
            genScript(sys.argv[1:])