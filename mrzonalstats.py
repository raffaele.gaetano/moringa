from sitsproc_c_modules import obiatools
from mtdUtils import *
from math import log10
import getopt
import time

stats_dict = {"count": obiatools.OSTAT_COUNT, \
              "sum": obiatools.OSTAT_SUM, \
              "mean": obiatools.OSTAT_MEAN, \
              "std": obiatools.OSTAT_STD, \
              "median": obiatools.OSTAT_MEDIAN, \
              "min": obiatools.OSTAT_MIN, \
              "max": obiatools.OSTAT_MAX, \
              "range": obiatools.OSTAT_RANGE, \
              "majority": obiatools.OSTAT_MAJORITY, \
              "minority": obiatools.OSTAT_MINORITY, \
              "variety": obiatools.OSTAT_VARIETY}

stats_fldn = {obiatools.OSTAT_COUNT: "cnt", \
              obiatools.OSTAT_SUM: "sum", \
              obiatools.OSTAT_MEAN: "m", \
              obiatools.OSTAT_STD: "s", \
              obiatools.OSTAT_MEDIAN: "med", \
              obiatools.OSTAT_MIN: "min", \
              obiatools.OSTAT_MAX: "max", \
              obiatools.OSTAT_RANGE: "rng", \
              obiatools.OSTAT_MAJORITY: "maj", \
              obiatools.OSTAT_MINORITY: "mno", \
              obiatools.OSTAT_VARIETY: "var"}


def cloneVectorDataStructure(ds_in, fname, ly = 0, epsg = None):
    ds_in_ly = ds_in.GetLayer(ly)

    if epsg is None:
        srs = ds_in_ly.GetSpatialRef()
    else:
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(epsg)

    drv = ogr.GetDriverByName('ESRI Shapefile')
    ds_out = drv.CreateDataSource(fname)
    ds_out_ly = ds_out.CreateLayer(os.path.splitext(os.path.basename(fname))[0],
                                   srs=srs,
                                   geom_type=ds_in_ly.GetLayerDefn().GetGeomType())
    #f = ds_in_ly.GetNextFeature()
    f = ds_in_ly[0]
    [ds_out_ly.CreateField(f.GetFieldDefnRef(i)) for i in range(f.GetFieldCount())]

    return ds_out


def generateGridBasedSubsets(in_vec, gridSize, epsg = None):
    ds_in = ogr.Open(in_vec)
    ds_in_ly = ds_in.GetLayer(0)
    ext = ds_in_ly.GetExtent()

    rect = []
    for x in np.arange(ext[0], ext[1], gridSize[0]):
        for y in np.arange(ext[2], ext[3], gridSize[1]):
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(x, y)
            ring.AddPoint(x + gridSize[0], y)
            ring.AddPoint(x + gridSize[0], y + gridSize[1])
            ring.AddPoint(x, y + gridSize[1])
            ring.AddPoint(x, y)
            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)
            rect.append(poly)

    NZ = int(log10(len(rect)) + 1)

    #Hardcoded size of bins
    s_rect_bins = 100
    rect_bins = [rect[t:t+s_rect_bins] for t in range(0,len(rect),s_rect_bins)]

    out_list = []

    for b in range(len(rect_bins)):

        ds_out = []

        for i in range(b*s_rect_bins, b*s_rect_bins+len(rect_bins[b])):
            out_list.append(os.path.splitext(in_vec)[0] + '_' + str(i + 1).zfill(NZ) + '.shp')
            ds_out.append(cloneVectorDataStructure(ds_in, out_list[i], epsg = epsg))

        ds_in_ly.ResetReading()

        for f in ds_in_ly:
            i = 0
            found = False
            while not found and i < len(rect_bins[b]):
                if f.GetGeometryRef().Intersects(rect[b*s_rect_bins + i]):
                    ds_out[i].GetLayer(0).CreateFeature(f)
                    found = True
                i += 1

        #[x.Destroy() for x in ds_out]
        ds_out = None

    ds_in = None

    return out_list


def mrExtentInfo(ras_ds, obj_ds):
    ras_geoT = ras_ds.GetGeoTransform()
    obj_geoT = obj_ds.GetGeoTransform()

    row_idx = np.empty(obj_ds.RasterYSize)
    col_idx = np.empty(obj_ds.RasterXSize)

    for i in range(obj_ds.RasterYSize):
        g = (obj_geoT[3] + (i + 0.5) * obj_geoT[5])
        row_idx[i] = int((g - ras_geoT[3]) / ras_geoT[5])

    for j in range(obj_ds.RasterXSize):
        g = (obj_geoT[0] + (j + 0.5) * obj_geoT[1])
        col_idx[j] = int((g - ras_geoT[0]) / ras_geoT[1])

    valid_ras_extent = [int(row_idx[0]), int(col_idx[0]), int(row_idx[-1] - row_idx[0] + 1),
                        int(col_idx[-1] - col_idx[0] + 1)]

    return valid_ras_extent, (row_idx - row_idx[0]).astype(np.int32), (col_idx - col_idx[0]).astype(np.int32)


def zonalStats(ras, obj_vec, stats, fld="Segment_ID", ras_name="R0", bnd_prefix=None, obj_ras=None, upsample=1.0,
               overwrite=False, nodata_value=-9999.0):
    """ zonalStats(ras, obj_vec, stats, fld="Segment_ID",ras_name="R0",bnd_prefix=None, obj_ras=None, upsampme=1.0)
    Compute zonal statistics on a raster using an object layer in vector format.
    If a raster (.tif) segmentation with the same filename exists, it is used as raster
    object layer, otherwise a rasterization is performed using a scale factor with
    respect to the size of the input raster.

    INPUT   :   ras             -   input raster
                obj_vec         -   segmentation (vector, needs 1 column with object IDs)
                stats           -   list of statistics to compute. Currently available:
                                    "count", "sum", "mean", "std", "median", "min", "max",
                                    "range", "majority", "minority", "variety".
                fld             -   Object ID field in shapefile (def. "Segment_ID")
                ras_name        -   Raster prefix for shapefile columns (def. "R0").
                                    Use short names! Max total fieldname length in .shp is 10 characters!
                bnd_prefix      -   Band prefix for shapefile columns (def. 'B' : B1, B2, ...).
                obj_ras         -   optional raster segmentation file.
                                    If None, first .tif file with the same name of obj_vec is sought.
                                    If not found, such raster will be generated.
                upsample        -   Scales raster size by this value to generate the raster segmentation.
                                    If None, rasterizes obj_vec on raster resolution
                overwrite       -   If True, overwrites existing fields with newly computed statistics.
                nodata_value    -   Value to consider as nodata when computing object statistics (skipped, def. -9999)

    OUTPUT  :   updates 'obj_vec' with one column per statistics per band.
    """
    if ras_name == "":
        ras_name = "R0"

    ras_ds = gdal.Open(ras)
    B = ras_ds.RasterCount

    if bnd_prefix is None:
        bnd_names = ['B' + str(x) for x in range(1, B + 1)]
    else:
        bnd_names = [bnd_prefix + str(x) for x in range(1, B + 1)]

    if obj_ras is None:
        obj_ras = os.path.splitext(obj_vec)[0] + '.tif'
        if not os.path.exists(obj_ras):
            rasterizeOnReference(obj_vec, fld, ras, obj_ras, scale_factor=upsample)

    oras_ds = gdal.Open(obj_ras)

    ext, ridx, cidx = mrExtentInfo(ras_ds, oras_ds)
    ras_arr = multiBandToArray(ras_ds, ext).astype(np.float32)
    oras_arr = bandToArray(oras_ds.GetRasterBand(1)).astype(np.int32)

    cstats = np.array([stats_dict[x] for x in stats]).astype(np.int32)
    zstats, stats_band_count = obiatools.zonalstats(oras_arr, ras_arr, ridx, cidx, cstats,np.array([nodata_value] * B, np.float32))

    ovec_ds = ogr.Open(obj_vec, 1)
    ovec_ly = ovec_ds.GetLayer(0)

    toSkip = []
    fields = []
    for s in range(0, len(stats_band_count)):
        for k in range(0, stats_band_count[s]):
            fields.append(ras_name + bnd_names[k] + stats_fldn[cstats[s]])
            if ovec_ly.FindFieldIndex(fields[-1], 1) == -1:
                toSkip.append(False)
                field = ogr.FieldDefn(fields[-1], ogr.OFTReal)
                ovec_ly.CreateField(field)
            else:
                toSkip.append(True)

    for i in range(0, len(zstats)):
        for feature in ovec_ly:
            oid = feature.GetField(fld)
            if oid in zstats:
                for s in range(0, len(fields)):
                    if overwrite is True or (overwrite is False and not toSkip[s]):
                        if zstats[oid][s] != nodata_value:
                            feature.SetField(fields[s], zstats[oid][s].astype(np.float64))
                        else:
                            feature.SetField(fields[s], nodata_value)
                ovec_ly.SetFeature(feature)

    ovec_ds.Destroy()

    return zstats, sum(toSkip)

def multiZonalStatsAsArray(ras_lists, obj_vec, stats, series_prefix='T', ras_prefix='R', bnd_prefix='B', nodata_value=-9999.0, fix_nodata=False):

    obj_ras = os.path.splitext(obj_vec)[0] + '.tif'
    if not os.path.exists(obj_ras):
        sys.exit("Raster reference for object layer " + obj_ras + " not found!")

    oras_ds = gdal.Open(obj_ras)

    NS = len(ras_lists)
    NL = [len(ras_lists[i]) for i in range(NS)]

    if series_prefix is None:
        series_prefix = 'S'
    series_names = [series_prefix + str(x) for x in range(1, NS + 1) for item in ras_lists[x-1]]
    ras_prefixes = [ras_prefix + str(l) for k in range(NS) for l in range(1, NL[k] + 1)]

    ras_list = [item for sublist in ras_lists for item in sublist]

    NR = len(ras_list)
    ras_names = [series_names[x] + ras_prefixes[x] for x in range(NR)]

    allobj = None
    allstats = None
    allfields = []

    oras_arr = bandToArray(oras_ds.GetRasterBand(1)).astype(np.int32)

    for i in range(NR):
        ras_ds = gdal.Open(ras_list[i])
        B = ras_ds.RasterCount
        bnd_names = [bnd_prefix + str(x) for x in range(1, B + 1)]

        ext, ridx, cidx = mrExtentInfo(ras_ds, oras_ds)
        #ras_arr = multiBandToArray(ras_ds, ext).astype(np.float32)
        ras_arr = bandToArray(ras_ds, ext).astype(np.float32)
        ras_ds = None

        cstats = np.array([stats_dict[x] for x in stats]).astype(np.int32)
        zstats, stats_band_count = obiatools.zonalstats(oras_arr, ras_arr, ridx, cidx, cstats,np.array([nodata_value] * B, np.float32))

        ras_arr = None

        for s in range(0, len(stats_band_count)):
            for k in range(0, stats_band_count[s]):
                allfields.append(ras_names[i] + bnd_names[k] + stats_fldn[cstats[s]])

        if i==0:
            allobj = np.array(list(zstats.keys()))
            allstats = np.array(list(zstats.values()))
        else:
            if len(allstats) > 0:
                allstats = np.append(allstats,np.array(list(zstats.values())),axis=1)

        zstats = None

    oras_arr = None
    oras_ds = None

    if fix_nodata is True and len(allstats) > 0:
        # R-like rough fix
        allstats[np.where(allstats == nodata_value)] = np.nan
        mns = np.tile(np.nanmean(allstats, axis=0), [len(allstats), 1])
        idx = np.isnan(allstats)
        allstats[idx] = mns[idx]

    return allobj,allfields,allstats


def updateVector(allstats,allfields,allobj,obj_vec,obj_fld="Segment_ID",overwrite=False):

    ovec_ds = ogr.Open(obj_vec, 1)
    ovec_ly = ovec_ds.GetLayer(0)

    toSkip = []
    for f in allfields:
        if ovec_ly.FindFieldIndex(f, 1) == -1:
            toSkip.append(False)
            field = ogr.FieldDefn(f, ogr.OFTReal)
            ovec_ly.CreateField(field)
        else:
            toSkip.append(True)

    ovec_ly.ResetReading()

    for feature in ovec_ly:
        oid = feature.GetField(obj_fld)
        idx = np.where(allobj==oid)[0]
        if len(idx) == 1:
            s = allstats[idx[0]]
            for i in range(len(s)):
                if overwrite is True or (overwrite is False and not toSkip[s]):
                    feature.SetField(allfields[i], s[i].astype(np.float64))
            ovec_ly.SetFeature(feature)

    ovec_ds = None

    return sum(toSkip)

def multiZonalStats(ras_list, obj_vec, stats, series_prefix='T', ras_prefix='R', bnd_prefix='B', nodata_value=-9999.0, fix_nodata=False,obj_fld="Segment_ID",overwrite=False):

    allobj, allfields, allstats = multiZonalStatsAsArray(ras_list,obj_vec,stats,series_prefix,ras_prefix,bnd_prefix,nodata_value,fix_nodata)
    nskip = updateVector(allstats, allfields, allobj, obj_vec, obj_fld, overwrite)
    allobj = None
    allstats = None
    return allfields, nskip

if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python mrzonalstats.py [--series-prefix <def. T>] [--raster-prefix <def. R>] [--band-prefix <def. B>] [--nodata-value <def. -9999>] [--fix-nodata] <raster list> <vector_layer> <object_id_field> <stats>\n' +
            'Usage: in raster list, separate series with colon (:) and rasters within series with comma (,)\n' +
            'Usage: in stats, separate statistics to compute with comma (,)')
    else:
        try:
            opts, args = getopt.getopt(sys.argv[1:], '', ['series-prefix=','raster-prefix=','band-prefix=','nodata-value=','fix-nodata','overwrite'])
        except getopt.GetoptError as err:
            sys.exit(err)

        sp = 'T'
        rp = 'R'
        bp = 'B'
        ndv = -9999.0
        fix = False
        owt = False

        # Parse options
        for opt, val in opts:
            if opt == '--series-prefix':
                sp = val
            elif opt == '--raster-prefix':
                rp = val
            elif opt == '--band-prefix':
                bp = val
            elif opt == '--nodata-value':
                ndv = float(val)
            elif opt == '--fix-nodata':
                fix = True
            elif opt == '--overwrite':
                owt = True

        ras_list = []
        tmp_list = args[0].split('::')
        [ras_list.append(x.split(',')) for x in tmp_list]

        obj_vec = args[1]
        obj_fld = args[2]
        stats = args[3].split(',')

        multiZonalStats(ras_list, obj_vec, stats, series_prefix=sp, ras_prefix=rp, bnd_prefix=bp,
                        nodata_value=ndv, fix_nodata=fix, obj_fld=obj_fld, overwrite=owt)
