import configparser
import fnmatch
import getopt
import glob
import os
import platform
import subprocess
import sys
import shutil
import warnings
import ogr
import csv

from math import floor,log10
from computeFeatures import featureComputation, readConfigFile
from segmentationWorkflow import segmentationWorkflow, generateGTSamples, generateVALSamples
from classificationWorkflow import training, classify
from mtdUtils import checkSRS, getRasterInfo, getFieldNames, keepFields, queuedProcess
from validationFramework import pixelValidation,surfaceValidation,formatValidationTxt, genKFolds, kFoldRefToSamples, kFoldReport, getVariableImportance
import time

def main(argv):
    try:
        opts, args = getopt.getopt(argv, '', ['runlevel=', 'single-step'])
    except getopt.GetoptError as err:
        sys.exit(err)

    cfg_fn = args[0]
    if not os.path.exists(cfg_fn):
        sys.exit("Invalid configuration file provided!")

    input_runlevel = 0
    current_runlevel = 0
    single_step = False

    for opt, val in opts:
        if opt == '--runlevel':
            input_runlevel = int(val)
        elif opt == '--single-step':
            single_step = True

    # Platform dependent parameters
    if platform.system() == 'Linux':
        sh = False
    elif platform.system() == 'Windows':
        sh = True
    else:
        sys.exit("Platform not supported!")

    ######################
    # Import config file #
    ######################

    config = configparser.ConfigParser()
    config.read(cfg_fn)
    base_fld = config.get('GENERAL CONFIGURATION', 'basefolder')
    # Search all Sentinel-2 and Landsat-8 images under base folder
    sits_L8 = []
    sits_S2 = []
    sits_S2L2A = []
    sits_S2THEIA = []
    sits_VenusTHEIA = []
    for r, d, f in os.walk(base_fld):
        for x in d:
            fld = r + '/' + x
            mdfL8 = glob.glob(fld + '/*_MTL*.txt')
            mdfS2L2A = glob.glob(fld + '/tileInfo.json') + glob.glob(fld + '/MTD_TL_MSIL2A.xml')
            mdfS2 = glob.glob(fld + '/tileInfo.json') + glob.glob(fld + '/metadata.xml')
            mdfS2THEIA = glob.glob(fld + '/*_MTD_ALL.xml')
            mdfVenusTHEIA = glob.glob(fld + '/*_L2VALD.HDR')
            if len(mdfL8) > 0:
                sits_L8.append(fld)
            elif len(mdfS2) > 0:
                sits_S2.append(fld)
            elif len(mdfS2L2A) > 0:
                sits_S2L2A.append(fld)
            elif len(mdfS2THEIA) > 0:
                sits_S2THEIA.append(fld)
            elif len(mdfVenusTHEIA) > 0:
                sits_VenusTHEIA.append(fld)

    #feat_pref = ['T1','T2','T3','T4']
    if len(sits_S2) > 0:
        #feat_pref.append('S2')
        sits_S2 = sorted(sits_S2)
    if len(sits_S2L2A) > 0:
        #feat_pref.append('S2')
        sits_S2L2A = sorted(sits_S2L2A)
    if len(sits_L8) > 0:
        #feat_pref.append('L8')
        sits_L8 = sorted(sits_L8)
    if len(sits_S2THEIA) > 0:
        #feat_pref.append('L8')
        sits_S2THEIA = sorted(sits_S2THEIA)
    if len(sits_VenusTHEIA) > 0:
        #feat_pref.append('L8')
        sits_VenusTHEIA = sorted(sits_VenusTHEIA)

    if len(sits_L8 + sits_S2 + sits_S2L2A + sits_S2THEIA +sits_VenusTHEIA) == 0:
        sys.exit('No time series available! Exiting.')

    ch_name = config.get('GENERAL CONFIGURATION', 'chainname')
    output_fld = base_fld + '/' + ch_name
    if not os.path.exists(output_fld):
        os.mkdir(output_fld)

    ch_mode = int(config.get('GENERAL CONFIGURATION', 'chainmode'))
    force_sample_generation = config.get('GENERAL CONFIGURATION', 'forcesamplegen') == 'YES'
    setup_name = config.get('GENERAL CONFIGURATION', 'setupname')
    N_proc = int(config.get('GENERAL CONFIGURATION', 'nprocesses'))

    ###############################################################
    # 0 - Preprocess VHR scene (TOA conversion/VRT/Pansharpening) #
    ###############################################################

    ms_fld = config.get('VHR SCENE CONFIGURATION', 'msfolder')
    pan_fld = config.get('VHR SCENE CONFIGURATION', 'panfolder')
    ps_fld = pan_fld + '/../PANSHARP'

    toa = config.get('VHR SCENE CONFIGURATION', 'toa') == 'YES'
    pansharp = config.get('VHR SCENE CONFIGURATION', 'pansharpening') == 'YES'

    ps_file = None

    if toa:
        ms_file = ms_fld + '/MS_TOA.vrt'
        pan_file = pan_fld + '/PAN_TOA.vrt'
    else:
        ms_file = ms_fld + '/MS.vrt'
        pan_file = pan_fld + '/PAN.vrt'

    if pansharp:
        ps_file = ps_fld + '/PANSHARP.tif'

    if input_runlevel < 1:

        fld = ms_fld

        # Remove pre-existing TOA files
        lst = glob.glob(fld + '/*_TOA.*')
        for f in lst:
            os.remove(f)

        lst = glob.glob(fld + '/*.tif')
        if platform.system() == 'Linux' and len(lst) == 0:
            lst = glob.glob(fld + '/*.TIF')
        if len(lst) == 0:
            sys.exit('No GeoTIFF in the selected MS folder')
        cmd = ['gdalbuildvrt', '-srcnodata', '0', '-vrtnodata', '0', '-overwrite', fld + '/MS.vrt'] + lst
        subprocess.call(cmd, shell=sh)

        if toa:
            for f in lst:
                fn = os.path.splitext(f)
                cmd = ['otbcli_OpticalCalibration', '-in', f, '-level', 'toa', '-out', fn[0] + '_TOApre.tif']
                subprocess.call(cmd, shell=sh)
                cmd = ['otbcli_BandMathX', '-il', fn[0] + '_TOApre.tif', '-exp', '10000*im1', '-out',
                       fn[0] + '_TOA.tif', 'uint16']
                subprocess.call(cmd, shell=sh)
                os.remove(fn[0] + '_TOApre.tif')
                if os.path.exists(fn[0] + '_TOApre.geom'):
                    os.remove(fn[0] + '_TOApre.geom')
            cmd = ['gdalbuildvrt', '-srcnodata', '0', '-vrtnodata', '0', '-overwrite', fld + '/MS_TOA.vrt'] + [
                os.path.splitext(f)[0] + '_TOA.tif' for f in lst]
            subprocess.call(cmd, shell=sh)

        fld = pan_fld

        # Remove pre-existing TOA files
        lst = glob.glob(fld + '/*_TOA.tif')
        for f in lst:
            os.remove(f)

        lst = glob.glob(fld + '/*.tif')
        if platform.system() == 'Linux' and len(lst) == 0:
            lst = glob.glob(fld + '/*.TIF')
        if len(lst) == 0:
            sys.exit('No GeoTIFF in the selected PAN folder')
        cmd = ['gdalbuildvrt', '-srcnodata', '0', '-vrtnodata', '0', '-overwrite', fld + '/PAN.vrt'] + lst
        subprocess.call(cmd, shell=sh)

        if toa:
            for f in lst:
                fn = os.path.splitext(f)
                cmd = ['otbcli_OpticalCalibration', '-in', f, '-level', 'toa', '-out', fn[0] + '_TOApre.tif']
                subprocess.call(cmd, shell=sh)
                cmd = ['otbcli_BandMathX', '-il', fn[0] + '_TOApre.tif', '-exp', '10000*im1', '-out',
                       fn[0] + '_TOA.tif', 'uint16']
                subprocess.call(cmd, shell=sh)
                os.remove(fn[0] + '_TOApre.tif')
                if os.path.exists(fn[0] + '_TOApre.geom'):
                    os.remove(fn[0] + '_TOApre.geom')
            cmd = ['gdalbuildvrt', '-srcnodata', '0', '-vrtnodata', '0', '-overwrite', fld + '/PAN_TOA.vrt'] + [
                os.path.splitext(f)[0] + '_TOA.tif' for f in lst]
            subprocess.call(cmd, shell=sh)

        if pansharp:
            if not os.path.exists(ps_fld):
                os.mkdir(ps_fld)
            cmd = ['otbcli_Superimpose', '-inr', pan_file, '-inm', ms_file, '-out', ps_fld + '/ms.tif', 'uint16']
            subprocess.call(cmd, shell=sh)
            cmd = ['otbcli_Pansharpening', '-inp', pan_file, '-inxs', ps_fld + '/ms.tif', '-out', ps_file, 'uint16',
                   '-method', 'bayes']
            subprocess.call(cmd, shell=sh)
            os.remove(ps_fld + '/ms.tif')

        if single_step:
            sys.exit("Single step mode. Exiting.")

    current_runlevel += 1

    ##########################################
    # 1 - VHR Scene cutting and segmentation #
    ##########################################

    pansharp = config.get('VHR SCENE CONFIGURATION', 'pansharpening') == 'YES'
    extent = config.get('GENERAL CONFIGURATION', 'extent')
    reference = config.get('GENERAL CONFIGURATION', 'reference')
    validation = config.get('GENERAL CONFIGURATION', 'validation')

    scale = config.get('VHR SCENE CONFIGURATION', 'scale')
    shape = str(1 - float(config.get('VHR SCENE CONFIGURATION', 'shape')))
    comp = config.get('VHR SCENE CONFIGURATION', 'compactness')
    meml = config.get('VHR SCENE CONFIGURATION', 'memlimit')

    ath = float(config.get('VHR SCENE CONFIGURATION', 'gtareathresh'))

    params = [scale, shape, comp, meml]

    if pansharp:
        seg_src = ps_file
    else:
        seg_src = ms_file # if neither TOA nor PANSHARP, provide manually a VRT named 'MS.vrt' with source image

    # Define here the target SRS based on VHSR image
    dst_srs = checkSRS(seg_src)
    dst_srs_code = dst_srs.split(':')[1]

    seg_fld = os.path.split(seg_src)[0] + '/../SEGMENTATION'
    seg_output = seg_fld + '/segmentation.tif'

    if input_runlevel < 2:

        if not os.path.exists(seg_output):
            tl = segmentationWorkflow(seg_fld, seg_src, extent, params, seg_output)
        else:
            tl = glob.glob(seg_fld + '/segmentation_*.shp')
        generateGTSamples(reference,tl,seg_fld,ath)
        if ch_mode == 0 or ch_mode == 2:
            generateVALSamples(validation, tl, seg_fld)
        elif ch_mode < 0:
            generateVALSamples(reference, tl, seg_fld)

        if single_step:
            sys.exit("Single step mode. Exiting.")

    current_runlevel += 1

    ##################################
    # 2 - Time Series Coregistration #
    ##################################

    if input_runlevel < 3:

        coreg = config.get('VHR-SITS COREGISTRATION', 'coregister') == 'YES'
        force_coreg = config.get('VHR-SITS COREGISTRATION', 'force') == 'YES'
        mxp = int(config.get('VHR-SITS COREGISTRATION', 'maxpoints'))

        if coreg:

            # Purge existing _COREG files
            if force_coreg:
                for r, d, f in os.walk(base_fld):
                    for fn in fnmatch.filter(f, '*_COREG.*'):
                        os.remove(os.path.join(r, fn))
                    for fn in fnmatch.filter(f, 'coreg*.geom'):
                        os.remove(os.path.join(r, fn))

            vhrbnd = config.get('VHR-SITS COREGISTRATION', 'vhrband')

            # Landsat 8
            ptrn = config.get('LANDSAT 8 CONFIGURATION', 'reflectance')
            bnd = config.get('VHR-SITS COREGISTRATION', 'l8band')
            ref_L8 = None
            for f in sits_L8:
                if not os.path.exists(f + '/coregistered.geom'):
                    refl = glob.glob(f + '/' + ptrn)[0]
                    if ref_L8 is None:
                        #Build reference for L8 time series
                        ref_L8 = f + '/../coreg_ref_L8.tif'
                        if not os.path.exists(ref_L8) or force_coreg:
                            cmd = ['otbcli_Superimpose', '-inr', refl, '-inm', ms_file, '-out', ref_L8, 'uint16']
                            subprocess.call(cmd, shell=sh)
                    cmd = ['python3', 'coregister.py', '--band-ref', vhrbnd, '--band-in', bnd, '--maxpoints', str(mxp), ref_L8, refl]
                    subprocess.call(cmd, shell=sh)

            # Sentinel-2
            ptrn = config.get('SENTINEL-2 CONFIGURATION', 'reflectance')
            bnd = config.get('VHR-SITS COREGISTRATION', 's2band')
            ref_S2 = None
            for f in sits_S2:
                if not os.path.exists(f + '/coregistered.geom'):
                    refl = glob.glob(f + '/' + ptrn)[0]
                    if ref_S2 is None:
                        # Build reference for S2 time series
                        ref_S2 = f + '/../coreg_ref_S2.tif'
                        if not os.path.exists(ref_S2) or force_coreg:
                            cmd = ['otbcli_Superimpose', '-inr', refl, '-inm', ms_file, '-out', ref_S2, 'uint16']
                            subprocess.call(cmd, shell=sh)
                    cmd = ['python3', 'coregister.py', '--band-ref', vhrbnd, '--band-in', bnd, '--maxpoints', str(mxp), ref_S2, refl]
                    subprocess.call(cmd, shell=sh)

            # Sentinel-2 MSIL2A
            ptrn = config.get('SENTINEL-2 CONFIGURATION', 'reflectance')
            bnd = config.get('VHR-SITS COREGISTRATION', 's2band')
            ref_S2L2A = None
            for f in sits_S2L2A:
                if not os.path.exists(f + '/coregistered.geom'):
                    refl = glob.glob(f + '/' + ptrn)[0]
                    if ref_S2L2A is None:
                        # Build reference for S2L2A time series
                        ref_S2L2A = f + '/../coreg_ref_S2L2A.tif'
                        if not os.path.exists(ref_S2L2A) or force_coreg:
                            cmd = ['otbcli_Superimpose', '-inr', refl, '-inm', ms_file, '-out', ref_S2L2A,
                                   'uint16']
                            subprocess.call(cmd, shell=sh)
                    cmd = ['python3', 'coregister.py', '--band-ref', vhrbnd, '--band-in', bnd, '--maxpoints',
                           str(mxp), ref_S2L2A, refl]
                    subprocess.call(cmd, shell=sh)

            # Sentinel-2 THEIA
            ptrn = config.get('SENTINEL-2 THEIA CONFIGURATION', 'reflectance')
            bnd = config.get('VHR-SITS COREGISTRATION', 's2band')
            ref_S2THEIA = None
            for f in sits_S2THEIA:
                if not os.path.exists(f + '/coregistered.geom'):
                    refl = glob.glob(f + '/' + ptrn)[0]
                    if ref_S2THEIA is None:
                        # Build reference for S2 time series
                        ref_S2THEIA = f + '/../coreg_ref_S2THEIA.tif'
                        if not os.path.exists(ref_S2THEIA) or force_coreg:
                            cmd = ['otbcli_Superimpose', '-inr', refl, '-inm', ms_file, '-out', ref_S2THEIA, 'uint16']
                            subprocess.call(cmd, shell=sh)
                    cmd = ['python3', 'coregister.py', '--band-ref', vhrbnd, '--band-in', bnd, '--maxpoints', str(mxp), ref_S2THEIA, refl]
                    subprocess.call(cmd, shell=sh)

            # Venus THEIA
            ptrn = config.get('VENUS THEIA CONFIGURATION', 'reflectance')
            bnd = config.get('VHR-SITS COREGISTRATION', 'venusband')
            ref_VenusTHEIA = None
            for f in sits_VenusTHEIA:
                if not os.path.exists(f + '/coregistered.geom'):
                    refl = glob.glob(f + '/' + ptrn)[0]
                    if ref_VenusTHEIA is None:
                        # Build reference for L8 time series
                        ref_VenusTHEIA = f + '/../coreg_ref_VenusTHEIA.tif'
                        if not os.path.exists(ref_VenusTHEIA) or force_coreg:
                            cmd = ['otbcli_Superimpose', '-inr', refl, '-inm', ms_file, '-out', ref_VenusTHEIA, 'uint16']
                            subprocess.call(cmd, shell=sh)
                    cmd = ['python3', 'coregister.py', '--band-ref', vhrbnd, '--band-in', bnd, '--maxpoints', str(mxp), ref_VenusTHEIA, refl]
                    subprocess.call(cmd, shell=sh)

        if single_step:
            sys.exit("Single step mode. Exiting.")

    current_runlevel += 1

    ##########################
    # 3 - Feature Extraction #
    ##########################

    feat_fld = base_fld + '/FEAT'

    if input_runlevel < 4:

        cfg_L8 = readConfigFile(cfg_fn, 'LANDSAT 8 CONFIGURATION')
        cfg_S2 = readConfigFile(cfg_fn, 'SENTINEL-2 CONFIGURATION')
        cfg_S2L2A = readConfigFile(cfg_fn, 'SENTINEL-2 CONFIGURATION')
        cfg_S2THEIA = readConfigFile(cfg_fn, 'SENTINEL-2 THEIA CONFIGURATION')
        cfg_VenusTHEIA = readConfigFile(cfg_fn, 'VENUS THEIA CONFIGURATION')
        if len(sits_L8) > 0:
            feat_L8 = featureComputation(sits_L8, cfg_L8, 'L8', feat_fld, extent=extent)
        if len(sits_S2) > 0:
            feat_S2 = featureComputation(sits_S2, cfg_S2, 'S2', feat_fld, extent=extent)
        if len(sits_S2L2A) > 0:
            feat_S2L2A = featureComputation(sits_S2L2A, cfg_S2L2A, 'S2_L2A', feat_fld, extent=extent)
        if len(sits_S2THEIA) > 0:
            feat_S2THEIA = featureComputation(sits_S2THEIA, cfg_S2THEIA, 'S2_THEIA', feat_fld, extent=extent)
        if len(sits_VenusTHEIA) > 0:
            feat_VenusTHEIA = featureComputation(sits_VenusTHEIA, cfg_VenusTHEIA, 'Venus_THEIA', feat_fld, extent=extent)

        if single_step:
            sys.exit("Single step mode. Exiting.")

    current_runlevel += 1

    ##########################
    #  4 - Zonal Statistics  #
    ##########################

    shpd = ogr.GetDriverByName('ESRI Shapefile')

    samples_fld = output_fld + '/GT_SAMPLES'
    if not os.path.exists(samples_fld):
        os.mkdir(samples_fld)
    test_fld = output_fld + '/SAMPLES'
    if not os.path.exists(test_fld) and (ch_mode > 0 or force_sample_generation is True):
        os.mkdir(test_fld)
    if ch_mode <= 0 or ch_mode == 2:
        val_fld = output_fld + '/VAL_SAMPLES'
        if not os.path.exists(val_fld):
            os.mkdir(val_fld)

    cfg_L8 = readConfigFile(cfg_fn, 'LANDSAT 8 CONFIGURATION')
    cfg_S2 = readConfigFile(cfg_fn, 'SENTINEL-2 CONFIGURATION')
    cfg_S2L2A = readConfigFile(cfg_fn, 'SENTINEL-2 CONFIGURATION')
    cfg_S2THEIA = readConfigFile(cfg_fn, 'SENTINEL-2 THEIA CONFIGURATION')
    cfg_VenusTHEIA = readConfigFile(cfg_fn, 'VENUS THEIA CONFIGURATION')
    n_feat_L8 = [a for a in cfg_L8['feat'].split(',') if a != '']
    n_feat_S2 = [a for a in cfg_S2['feat'].split(',') if a != '']
    n_feat_S2L2A = [a for a in cfg_S2L2A['feat'].split(',') if a != '']
    n_feat_S2THEIA = [a for a in cfg_S2THEIA['feat'].split(',') if a != '']
    n_feat_VenusTHEIA = [a for a in cfg_VenusTHEIA['feat'].split(',') if a != '']

    if len(sits_L8) > 0 and len(n_feat_L8) > 0:
        with open(glob.glob(base_fld + '/FEAT/L8_FEAT/' + 'input_dates_*.txt')[0]) as df:
            dates_L8 = df.read().splitlines()
    else:
        n_feat_L8 = []
    if len(sits_S2) > 0 and len(n_feat_S2) > 0:
        with open(glob.glob(base_fld + '/FEAT/S2_FEAT/' + 'input_dates_*.txt')[0]) as df:
            dates_S2 = df.read().splitlines()
    else:
        n_feat_S2 = []
    if len(sits_S2L2A) > 0 and len(n_feat_S2L2A) > 0:
        with open(glob.glob(base_fld + '/FEAT/S2_L2A_FEAT/' + 'input_dates_*.txt')[0]) as df:
            dates_S2L2A = df.read().splitlines()
    else:
        n_feat_S2L2A = []
    if len(sits_S2THEIA) > 0 and len(n_feat_S2THEIA) > 0:
        with open(glob.glob(base_fld + '/FEAT/S2_THEIA_FEAT/' + 'input_dates_*.txt')[0]) as df:
            dates_S2THEIA = df.read().splitlines()
    else:
        n_feat_S2THEIA = []
    if len(sits_VenusTHEIA) > 0 and len(n_feat_VenusTHEIA) > 0:
        with open(glob.glob(base_fld + '/FEAT/Venus_THEIA_FEAT/' + 'input_dates_*.txt')[0]) as df:
            dates_VenusTHEIA = df.read().splitlines()
    else:
        n_feat_VenusTHEIA = []

    feat_L8_check = []
    feat_S2_check = []
    feat_S2L2A_check = []
    feat_S2THEIA_check = []
    feat_VenusTHEIA_check = []

    feat_to_vars = []

    for f in n_feat_L8:
        # Check presence of current feature
        fn = glob.glob(base_fld + '/FEAT/L8_FEAT/*_' + f + '_GAPF.tif')
        if len(fn) == 1 and cfg_L8['gapfillingmode'] != 'NONE':
            feat_L8_check.append(fn[0])
            [feat_to_vars.append('L8_' + f + '_' + x + '_GAPF') for x in dates_L8]
        else:
            fn = glob.glob(base_fld + '/FEAT/L8_FEAT/*_' + f + '.tif')
            if len(fn) == 1:
                if cfg_L8['gapfillingmode'] != 'NONE':
                    warnings.warn("L8: Feature " + f + " requested with GAPF, found without. Taking NOGAPF.")
                feat_L8_check.append(fn[0])
                [feat_to_vars.append('L8_' + f + '_' + x + '_NOGAPF') for x in dates_L8]
            else:
                warnings.warn("L8: Feature " + f + " listed in config file but not found. Skipping.")
                continue

    for f in n_feat_S2:
        # Check presence of current feature
        fn = glob.glob(base_fld + '/FEAT/S2_FEAT/*_' + f + '_GAPF.tif')
        if len(fn) == 1 and cfg_S2['gapfillingmode'] != 'NONE':
            feat_S2_check.append(fn[0])
            [feat_to_vars.append('S2_' + f + '_' + x + '_GAPF') for x in dates_S2]
        else:
            fn = glob.glob(base_fld + '/FEAT/S2_FEAT/*_' + f + '.tif')
            if len(fn) == 1:
                if cfg_S2['gapfillingmode'] != 'NONE':
                    warnings.warn("S2: Feature " + f + " requested with GAPF, found without. Taking NOGAPF.")
                feat_S2_check.append(fn[0])
                [feat_to_vars.append('S2_' + f + '_' + x + '_NOGAPF') for x in dates_S2]
            else:
                warnings.warn("S2: Feature " + f + " listed in config file but not found. Skipping.")
                continue

    for f in n_feat_S2L2A:
        # Check presence of current feature
        fn = glob.glob(base_fld + '/FEAT/S2_L2A_FEAT/*_' + f + '_GAPF.tif')
        if len(fn) == 1 and cfg_S2L2A['gapfillingmode'] != 'NONE':
            feat_S2L2A_check.append(fn[0])
            [feat_to_vars.append('S2-L2A_' + f + '_' + x + '_GAPF') for x in dates_S2L2A]
        else:
            fn = glob.glob(base_fld + '/FEAT/S2_L2A_FEAT/*_' + f + '.tif')
            if len(fn) == 1:
                if cfg_S2L2A['gapfillingmode'] != 'NONE':
                    warnings.warn("S2_L2A: Feature " + f + " requested with GAPF, found without. Taking NOGAPF.")
                feat_S2L2A_check.append(fn[0])
                [feat_to_vars.append('S2-L2A_' + f + '_' + x + '_NOGAPF') for x in dates_S2L2A]
            else:
                warnings.warn("S2_L2A: Feature " + f + " listed in config file but not found. Skipping.")
                continue

    for f in n_feat_S2THEIA:
        # Check presence of current feature
        fn = glob.glob(base_fld + '/FEAT/S2_THEIA_FEAT/*_' + f + '_GAPF.tif')
        if len(fn) == 1 and cfg_S2THEIA['gapfillingmode'] != 'NONE':
            feat_S2THEIA_check.append(fn[0])
            [feat_to_vars.append('S2-L2A-THEIA_' + f + '_' + x + '_GAPF') for x in dates_S2THEIA]
        else:
            fn = glob.glob(base_fld + '/FEAT/S2_THEIA_FEAT/*_' + f + '.tif')
            if len(fn) == 1:
                if cfg_S2THEIA['gapfillingmode'] != 'NONE':
                    warnings.warn("S2_THEIA: Feature " + f + " requested with GAPF, found without. Taking NOGAPF.")
                feat_S2THEIA_check.append(fn[0])
                [feat_to_vars.append('S2-L2A-THEIA_' + f + '_' + x + '_NOGAPF') for x in dates_S2THEIA]
            else:
                warnings.warn("S2_THEIA: Feature " + f + " listed in config file but not found. Skipping.")
                continue

    for f in n_feat_VenusTHEIA:
        # Check presence of current feature
        fn = glob.glob(base_fld + '/FEAT/Venus_THEIA_FEAT/*_' + f + '_GAPF.tif')
        if len(fn) == 1 and cfg_VenusTHEIA['gapfillingmode'] != 'NO':
            feat_VenusTHEIA_check.append(fn[0])
            [feat_to_vars.append('VENUS-L2A-THEIA_' + f + '_' + x + '_GAPF') for x in dates_VenusTHEIA]
        else:
            fn = glob.glob(base_fld + '/FEAT/Venus_THEIA_FEAT/*_' + f + '.tif')
            if len(fn) == 1:
                if cfg_VenusTHEIA['gapfillingmode'] != 'NO':
                    warnings.warn("VENUS_THEIA: Feature " + f + " requested with GAPF, found without. Taking NOGAPF.")
                feat_VenusTHEIA_check.append(fn[0])
                [feat_to_vars.append('VENUS-L2A-THEIA_' + f + '_' + x + '_NOGAPF') for x in dates_VenusTHEIA]
            else:
                warnings.warn("VENUS_THEIA: Feature " + f + " listed in config file but not found. Skipping.")
                continue

    # Ingesting user features (warning: analysis ready raster needed - good projection, size, etc.)
    feat_USER = []
    cfg_USER = readConfigFile(cfg_fn, 'ADDITIONAL FEATURES')
    ufl = cfg_USER['userfeatlist']
    if ufl != '':
        feat_USER = ufl.split(',')
        for ufn in feat_USER:
            if not os.path.exists(ufn):
                sys.exit('File ' + ufn + ' not found! Exiting.')
            else:
                Nb, _ = getRasterInfo(ufn)
                nm = os.path.basename(os.path.splitext(ufn)[0])
                [feat_to_vars.append('USER_' + nm + '_' + str(x + 1)) for x in range(Nb)]

    if input_runlevel < 5:

        feat_L8 = []
        feat_S2 = []
        feat_S2L2A = []
        feat_S2THEIA = []
        feat_VENUSTHEIA = []

        for fx in feat_L8_check:
            # Reproject if needed
            if checkSRS(fx) != dst_srs:
                ff = fx.replace('.tif', '_rep.tif')
                cmd = ['gdalwarp', '-t_srs', dst_srs, fx, ff]
                subprocess.call(cmd, shell=sh)
                feat_L8.append(ff)
            else:
                feat_L8.append(fx)

        for fx in feat_S2_check:
            # Reproject if needed
            if checkSRS(fx) != dst_srs:
                ff = fx.replace('.tif', '_rep.tif')
                cmd = ['gdalwarp', '-t_srs', dst_srs, fx, ff]
                subprocess.call(cmd, shell=sh)
                feat_S2.append(ff)
            else:
                feat_S2.append(fx)

        for fx in feat_S2L2A_check:
            # Reproject if needed
            if checkSRS(fx) != dst_srs:
                ff = fx.replace('.tif', '_rep.tif')
                cmd = ['gdalwarp', '-t_srs', dst_srs, fx, ff]
                subprocess.call(cmd, shell=sh)
                feat_S2L2A.append(ff)
            else:
                feat_S2L2A.append(fx)

        for fx in feat_S2THEIA_check:
            # Reproject if needed
            if checkSRS(fx) != dst_srs:
                ff = fx.replace('.tif', '_rep.tif')
                cmd = ['gdalwarp', '-t_srs', dst_srs, fx, ff]
                subprocess.call(cmd, shell=sh)
                feat_S2THEIA.append(ff)
            else:
                feat_S2THEIA.append(fx)

        for fx in feat_VenusTHEIA_check:
            # Reproject if needed
            if checkSRS(fx) != dst_srs:
                ff = fx.replace('.tif', '_rep.tif')
                cmd = ['gdalwarp', '-t_srs', dst_srs, fx, ff]
                subprocess.call(cmd, shell=sh)
                feat_VENUSTHEIA.append(ff)
            else:
                feat_VENUSTHEIA.append(fx)

        feat_groups = []
        if len(feat_L8) > 0:
            feat_groups.append(feat_L8)
        if len(feat_S2) > 0:
            feat_groups.append(feat_S2)
        if len(feat_S2L2A) > 0:
            feat_groups.append(feat_S2L2A)
        if len(feat_S2THEIA) > 0:
            feat_groups.append(feat_S2THEIA)
        if len(feat_VENUSTHEIA) > 0:
            feat_groups.append(feat_VENUSTHEIA)
        if len(feat_USER) > 0:
            feat_groups.append(feat_USER)

        feat_groups_str = '::'.join([','.join(x) for x in feat_groups])

        # generating GT samples and merging tiles
        if os.path.exists(samples_fld + '/GT_samples.shp'):
            warnings.warn('File ' + samples_fld + '/GT_samples.shp' + ' already exists, skipping zonal stats on reference.')
        else:
            gttl = sorted(glob.glob(seg_fld + '/GT_segmentation_*.shp'))
            ogttl = []

            ext_flds = getFieldNames(gttl[0])

            # BEGIN ** GT_SAMPLES **
            cmd_list = []

            for tl in gttl:
                tocp = glob.glob(os.path.splitext(tl)[0] + '.*')
                for fn in tocp:
                    shutil.copyfile(fn, samples_fld + '/' + os.path.basename(fn))
                otl = samples_fld + '/' + os.path.basename(tl)
                ogttl.append(otl)
                cmd_list.append(
                    ['python3', 'mrzonalstats.py', '--series-prefix', 'T', '--raster-prefix', 'F', '--band-prefix', 'D',
                     '--fix-nodata', '--overwrite', feat_groups_str, otl, "Segment_ID", 'mean'])

            queuedProcess(cmd_list, N_processes=1, shell=sh)
            # END ** GT_SAMPLES **

            var_keys = [x for x in getFieldNames(ogttl[0]) if x not in ext_flds]

            with open(samples_fld + '/field_names.csv', 'w') as varfile:
                vw = csv.writer(varfile)
                for x in zip(var_keys, feat_to_vars):
                    vw.writerow(x)

            cmd_base = ['ogr2ogr', '-f', 'ESRI Shapefile']

            cmd = cmd_base + [samples_fld + '/GT_samples.shp', ogttl[0]]
            subprocess.call(cmd, shell=sh)

            for k in range(1, len(ogttl)):
                cmd = cmd_base + ['-update', '-append', samples_fld + '/GT_samples.shp', ogttl[k], '-nln', 'GT_samples']
                subprocess.call(cmd, shell=sh)

            for otl in ogttl:
                shpd.DeleteDataSource(otl)
                os.remove(otl.replace('.shp','.tif'))

        if ch_mode <= 0 or ch_mode == 2:
            # generating VAL samples and merging tiles
            gttl = sorted(glob.glob(seg_fld + '/VAL_segmentation_*.shp'))
            ogttl = []

            # BEGIN ** VAL_SAMPLES **
            cmd_list = []

            for tl in gttl:
                tocp = glob.glob(os.path.splitext(tl)[0] + '.*')
                for fn in tocp:
                    shutil.copyfile(fn, val_fld + '/' + os.path.basename(fn))
                otl = val_fld + '/' + os.path.basename(tl)
                ogttl.append(otl)
                cmd_list.append(
                    ['python3', 'mrzonalstats.py', '--series-prefix', 'T', '--raster-prefix', 'F', '--band-prefix', 'D',
                     '--fix-nodata', '--overwrite', feat_groups_str, otl, "Segment_ID", 'mean'])

            queuedProcess(cmd_list, N_processes=1, shell=sh)
            # END ** VAL_SAMPLES **

            cmd_base = ['ogr2ogr', '-f', 'ESRI Shapefile']

            cmd = cmd_base + [val_fld + '/VAL_samples.shp', ogttl[0]]
            subprocess.call(cmd, shell=sh)

            for k in range(1, len(ogttl)):
                cmd = cmd_base + ['-update', '-append', val_fld + '/VAL_samples.shp', ogttl[k], '-nln', 'VAL_samples']
                subprocess.call(cmd, shell=sh)

            for otl in ogttl:
                shpd.DeleteDataSource(otl)
                os.remove(otl.replace('.shp', '.tif'))

        if ch_mode > 0 or force_sample_generation is True:
            # generating map samples (TBD)
            gttl = sorted(glob.glob(seg_fld + '/segmentation_*.shp'))
            ogttl = []

            # BEGIN ** SAMPLES **
            cmd_list = []

            for tl in gttl:
                tocp = glob.glob(os.path.splitext(tl)[0] + '.*')
                for fn in tocp:
                    shutil.copyfile(fn, test_fld + '/' + os.path.basename(fn))
                otl = test_fld + '/' + os.path.basename(tl)
                ogttl.append(otl)
                cmd_list.append(
                    ['python3', 'mrzonalstats.py', '--series-prefix', 'T', '--raster-prefix', 'F', '--band-prefix', 'D',
                     '--fix-nodata', '--overwrite', feat_groups_str, otl, "Segment_ID", 'mean'])

            queuedProcess(cmd_list, N_processes=N_proc, shell=sh)
            # END ** SAMPLES **

        if single_step:
            sys.exit("Single step mode. Exiting.")

    current_runlevel += 1

    ##########################
    #  5 - Model Training    #
    ##########################

    cfieldlist = config.get('TRAINING CONFIGURATION', 'classcode').split(',')
    params = config.get('TRAINING CONFIGURATION', 'parameters').split(' ')
    rfimp = config.get('TRAINING CONFIGURATION', 'rfimportance') == 'YES'
    rfimpntrees = int(config.get('TRAINING CONFIGURATION', 'rfimpntrees'))
    rfimpnodesize = int(config.get('TRAINING CONFIGURATION', 'rfimpnodesize'))
    rfimpmaxfeat = int(config.get('TRAINING CONFIGURATION', 'rfimpmaxfeat'))
    rfimpnruns = int(config.get('TRAINING CONFIGURATION', 'rfimpnruns'))
    hierarchical_classif = config.get('TRAINING CONFIGURATION','hierarchicalclassif') == 'YES'

    model_fld = output_fld + '/MODEL_' + setup_name
    if not os.path.exists(model_fld):
        os.mkdir(model_fld)

    var_list = []
    with open(samples_fld + '/field_names.csv', 'r') as varfile:
        rdr = csv.reader(varfile)
        for row in rdr:
            if row[1] in feat_to_vars:
                var_list.append(row[0])

    if input_runlevel < 6:
        if hierarchical_classif is False:
            if ch_mode < 0:

                nFolds = -ch_mode
                dgt = int(floor(log10(nFolds)) + 1)

                train_folds, test_folds = genKFolds(reference, cfieldlist[-1], nFolds)
                kfold_train_samples, kfold_test_samples = kFoldRefToSamples(samples_fld + '/GT_samples.shp', val_fld + '/VAL_samples.shp', train_folds, test_folds)

                for cfield in cfieldlist:

                    accuracies = []
                    kappas = []
                    fscores = {}

                    for i in range(nFolds):

                        mfld = model_fld + '/fold_' + str(i+1).zfill(dgt)

                        if not os.path.exists(mfld):
                            os.mkdir(mfld)

                        sfn, mfn = training(kfold_train_samples[i], cfield, mfld, params, var_list)
                        val_list = [kfold_test_samples[i]]
                        val_out = mfld + '/' + os.path.basename(kfold_test_samples[i]).replace('.shp','_' + cfield + '.shp')
                        val_out_check = mfld + '/' + os.path.basename(kfold_test_samples[i]).replace('.shp','_' + cfield + '_check.shp')
                        val_out_tmp_list = classify(val_list, 'p' + cfield, sfn, mfn, mfld,
                                                    '_' + cfield + '_tmp', var_list)
                        keepFields(val_out_tmp_list[0], val_out, ['Segment_ID', cfield, 'p' + cfield, 'confidence'])
                        shpd.DeleteDataSource(val_out_tmp_list[0])
                        txt_out = mfld + '/' + os.path.basename(kfold_test_samples[i]).replace('.shp','_' + cfield + '_report.txt')
                        classes, cm, acc, kappa, prf = surfaceValidation(test_folds[i], val_out, val_out_check, cfield)
                        formatValidationTxt(classes, cm, acc, kappa, prf, txt_out)

                        accuracies.append(acc)
                        kappas.append(kappa)
                        for c,fs in zip(classes,prf[2]):
                            if c not in list(fscores.keys()):
                                fscores[c] = []
                            fscores[c].append(fs)

                    kFoldReport(fscores,accuracies,kappas,model_fld + '/kFold_report_' + cfield + '.txt')

            for cfield in cfieldlist:
                training(samples_fld + '/GT_samples.shp',cfield,model_fld,params,var_list)

            if rfimp == True:
                for cfield in cfieldlist:
                    getVariableImportance(samples_fld + '/GT_samples.shp',var_list,cfield,samples_fld + '/var_importance_' + cfield + '.csv', nbtrees=rfimpntrees, nodesize=rfimpnodesize, mtry=rfimpmaxfeat, nruns=rfimpnruns, field_names=samples_fld + '/field_names.csv')
        else :
            from classificationWorkflow import Htraining, Hclassify
            if ch_mode < 0:

                nFolds = -ch_mode
                dgt = int(floor(log10(nFolds)) + 1)
                train_folds, test_folds = genKFolds(reference, cfieldlist[-1], nFolds)
                kfold_train_samples, kfold_test_samples = kFoldRefToSamples(samples_fld + '/GT_samples.shp',
                                                                            val_fld + '/VAL_samples.shp', train_folds,
                                                                            test_folds)

                accuracies = {}
                kappas = {}
                fscores = {}

                for cfield in cfieldlist:
                    accuracies[cfield] = []
                    kappas[cfield] = []
                    fscores[cfield] = {}

                for i in range(nFolds):

                    mfld = model_fld + '/fold_' + str(i + 1).zfill(dgt)

                    if not os.path.exists(mfld):
                        os.mkdir(mfld)
                    sfn, mfn = Htraining(kfold_train_samples[i], cfieldlist, mfld, params, var_list)
                    val_list = [kfold_test_samples[i]]
                    val_out_tmp_list = Hclassify(val_list, sfn, mfn, var_list,
                                                mfld,'_h_classif')
                    for cfield in cfieldlist:
                        val_out = mfld + '/' + os.path.basename(kfold_test_samples[i]).replace('.shp',
                                                                                                 '_' + cfield +'.shp')
                        val_out_check = mfld + '/' + os.path.basename(kfold_test_samples[i]).replace('.shp',
                                                                                                 '_' + cfield +'_check.shp')

                        keepFields(val_out_tmp_list[0], val_out, ['Segment_ID', cfield, 'p' + cfield, 'confidence'])
                        txt_out = mfld + '/' + os.path.basename(kfold_test_samples[i]).replace('.shp',
                                                                                               '_' + cfield + '_report.txt')

                        classes, cm, acc, kappa, prf = surfaceValidation(test_folds[i], val_out, val_out_check, cfield)
                        formatValidationTxt(classes, cm, acc, kappa, prf, txt_out)

                        accuracies[cfield].append(acc)
                        kappas[cfield].append(kappa)
                        for c, fs in zip(classes, prf[2]):
                            if c not in list(fscores[cfield].keys()):
                                fscores[cfield][c] = []
                            fscores[cfield][c].append(fs)

                for cfield in cfieldlist:
                    kFoldReport(fscores[cfield], accuracies[cfield], kappas[cfield], model_fld + '/kFold_report_' + cfield + '.txt')

            Htraining(samples_fld + '/GT_samples.shp', cfieldlist, model_fld, params, var_list)

            if rfimp == True:
                for cfield in cfieldlist:
                    getVariableImportance(samples_fld + '/GT_samples.shp', var_list, cfield,
                                          model_fld + '/var_importance_' + cfield + '.csv', nbtrees=rfimpntrees,
                                          nodesize=rfimpnodesize, mtry=rfimpmaxfeat, nruns=rfimpnruns,
                                          field_names=samples_fld + '/field_names.csv')

        if single_step:
            sys.exit("Single step mode. Exiting.")

    current_runlevel += 1

    ##########################
    #  6 - Classification    #
    ##########################

    rasout = config.get('CLASSIFICATION OUTPUT CONFIGURATION', 'rasterize')
    final_fld = output_fld + '/CLASSIFICATION_' + setup_name
    if not os.path.exists(final_fld):
        os.mkdir(final_fld)

    comp_conf = config.get('CLASSIFICATION OUTPUT CONFIGURATION', 'confidence') == 'YES'

    if '-classifier' in params:
        classifier = params[params.index('-classifier') + 1]
    else:
        classifier = 'libsvm'
    stat_file = model_fld + '/GT_stats.xml'

    shpd = ogr.GetDriverByName('ESRI Shapefile')

    if input_runlevel < 7:
        if hierarchical_classif is False :
            for cfield in cfieldlist:
                model_file = model_fld + '/' + classifier + '_' + cfield + '.model'
                if not os.path.exists(model_file):
                    warnings.warn('Error: Model file ' + model_file + ' not found. Skipping.')
                    continue

                # Validation step
                if ch_mode == 0 or ch_mode == 2:
                    ref_shp = config.get('GENERAL CONFIGURATION', 'validation')
                    val_mode = int(config.get('GENERAL CONFIGURATION', 'validmode'))
                    val_list = [val_fld + '/VAL_samples.shp']
                    val_out = final_fld + '/VAL_samples_' + cfield + '.shp'
                    val_out_check = final_fld + '/VAL_samples_' + cfield + '_check.shp'
                    val_out_tmp_list = classify(val_list, 'p'+cfield, stat_file, model_file, final_fld, '_' + cfield + '_tmp', var_list, compute_confidence=comp_conf)
                    keepFields(val_out_tmp_list[0],val_out,['Segment_ID',cfield,'p'+cfield,'confidence'])
                    shpd.DeleteDataSource(val_out_tmp_list[0])
                    txt_out = final_fld + '/VAL_samples.' + cfield + '.report.txt'
                    if val_mode == 0:
                        pixelValidation(ref_shp, val_out, seg_output, txt_out, cfield)
                    elif val_mode == 1:
                        classes,cm,acc,kappa,prf = surfaceValidation(ref_shp, val_out, val_out_check, cfield)
                        formatValidationTxt(classes, cm, acc, kappa, prf, txt_out)

                # Classification and map production steps
                shp_list = glob.glob(test_fld + '/segmentation_*.shp')
                if ch_mode > 0 or (ch_mode < 0 and len(shp_list) > 0):
                    if not os.path.exists(final_fld + '/MAPS'):
                        os.mkdir(final_fld + '/MAPS')
                    if not os.path.exists(final_fld + '/MAPS/VECTOR_' + cfield):
                        os.mkdir(final_fld + '/MAPS/VECTOR_' + cfield)
                    map_list = []
                    ref_list = []

                    for cshp in shp_list:
                        ref_list.append(cshp.replace('.shp', '.tif'))

                    map_tmp_list = classify(shp_list,'p'+cfield,stat_file,model_file,final_fld + '/MAPS/VECTOR_' + cfield + '/','_' + cfield + '_tmp',var_list,Nproc=N_proc,compute_confidence=comp_conf)

                    for cshp in map_tmp_list:
                        map_out = cshp.replace('_tmp.shp','.shp')
                        keepFields(cshp,map_out,['Segment_ID','p'+cfield,'confidence'])
                        shpd.DeleteDataSource(cshp)
                        map_list.append(map_out)

                    if rasout == 'VRT':
                        if not os.path.exists(final_fld + '/MAPS/RASTER_' + cfield):
                            os.mkdir(final_fld + '/MAPS/RASTER_' + cfield)
                        ras_list = []
                        cmd_list = []
                        for map,ref in zip(map_list,ref_list):
                            ras_list.append(final_fld + '/MAPS/RASTER_' + cfield + '/' + os.path.basename(map).replace('.shp', '.tif'))
                            cmd = ['otbcli_Rasterization', '-in', map, '-im', ref, '-mode', 'attribute', '-mode.attribute.field', 'p'+cfield, '-out', ras_list[-1]]
                            cmd_list.append(cmd)
                        queuedProcess(cmd_list,N_processes=N_proc,shell=sh)

                        cmd = ['gdalbuildvrt', '-srcnodata', '0', '-vrtnodata', '0', final_fld + '/MAPS/RASTER_' + cfield + '/Classif_' + cfield + '.vrt'] + ras_list
                        subprocess.call(cmd, shell=sh)

        else :
            from classificationWorkflow import Hclassify
            h_model_fld = os.path.join(model_fld,'H-MODEL_' + '_'.join(cfieldlist))
            if len(glob.glob(os.path.join(h_model_fld,'*.model'))) == 0 :
                sys.exit('Error: There is no model file in ' + h_model_fld + ' folder. Skipping.')

            # Validation step
            if ch_mode == 0 or ch_mode == 2:
                ref_shp = config.get('GENERAL CONFIGURATION', 'validation')
                val_mode = int(config.get('GENERAL CONFIGURATION', 'validmode'))
                val_list = [val_fld + '/VAL_samples.shp']
                val_out = final_fld + '/VAL_samples_' + '_'.join(cfieldlist) + '.shp'
                val_out_check = final_fld + '/VAL_samples_' + '_'.join(cfieldlist) + '_check.shp'
                val_out_tmp_list = Hclassify(val_list,stat_file,h_model_fld,var_list,final_fld,'_h_classif')
                keepFields(val_out_tmp_list[0],val_out,['Segment_ID']+cfieldlist+['p'+c for c in cfieldlist]+['confidence'])
                shpd.DeleteDataSource(val_out_tmp_list[0])
                for cfield in cfieldlist:
                    txt_out = final_fld + '/VAL_samples.' + cfield + '.report.txt'
                    if val_mode == 0:
                        pixelValidation(ref_shp, val_out, seg_output, txt_out, cfield)
                    elif val_mode == 1:
                        classes,cm,acc,kappa,prf = surfaceValidation(ref_shp, val_out, val_out_check, cfield)
                        formatValidationTxt(classes, cm, acc, kappa, prf, txt_out)

            # Classification and map production steps
            shp_list = glob.glob(test_fld + '/segmentation_*.shp')
            if ch_mode > 0 or (ch_mode < 0 and len(shp_list) > 0):
                if not os.path.exists(final_fld + '/MAPS'):
                    os.mkdir(final_fld + '/MAPS')
                if not os.path.exists(final_fld + '/MAPS/VECTOR'):
                    os.mkdir(final_fld + '/MAPS/VECTOR')
                map_list = []
                ref_list = []
                for cshp in shp_list:
                    ref_list.append(cshp.replace('.shp', '.tif'))

                map_tmp_list = Hclassify(shp_list,stat_file,h_model_fld,var_list,os.path.join(final_fld, 'MAPS', 'VECTOR'),'_hclassif_tmp')
                class_field = []
                for cfield in cfieldlist:
                    class_field.append('p'+cfield)
                for cshp in map_tmp_list:
                    map_out = cshp.replace('_tmp.shp','.shp')
                    keepFields(cshp,map_out,['Segment_ID']+class_field+['confidence'])
                    shpd.DeleteDataSource(cshp)
                    map_list.append(map_out)

                if rasout == 'VRT':
                    for cfield in cfieldlist :
                        if not os.path.exists(final_fld + '/MAPS/RASTER_' + cfield):
                            os.mkdir(final_fld + '/MAPS/RASTER_' + cfield)
                        ras_list = []
                        cmd_list = []
                        for map,ref in zip(map_list,ref_list):
                            ras_list.append(final_fld + '/MAPS/RASTER_' + cfield + '/' + os.path.basename(map).replace('.shp', '.tif'))
                            cmd = ['otbcli_Rasterization', '-in', map, '-im', ref, '-mode', 'attribute', '-mode.attribute.field', 'p'+cfield, '-out', ras_list[-1]]
                            cmd_list.append(cmd)
                        queuedProcess(cmd_list,N_processes=N_proc,shell=sh)
                        cmd = ['gdalbuildvrt', '-srcnodata', '0', '-vrtnodata', '0', final_fld + '/MAPS/RASTER_' + cfield + '/Classif_' + cfield + '.vrt'] + ras_list
                        subprocess.call(cmd, shell=sh)
        if single_step:
            sys.exit("Single step mode. Exiting.")

    return current_runlevel


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(
            'Usage: python3 launchChain.py [--runlevel < n >] [--single-step] <config-file>')
    else:
        main(sys.argv[1:])
